<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cuenta;
use App\Models\MovCuenta;
use App\Models\OrdenPago;
use App\Models\Proveedor;
use App\Models\DetalleOrdenPago;
use Illuminate\Support\Facades\DB;
use App\Models\Pago;
use Exception;
use PDF;
use NumeroALetras;


class OrdenPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        
        $ordenes = OrdenPago::orderBy('CODIGO','DESC')->orderBy('CODIGO_BIS','DESC')->get();

        if(isset($request['id'])){
            $id = $request['id'];
            return view('orden_pago.index', compact('ordenes','id'));
        }else{
            return view('orden_pago.index', compact('ordenes'));
        }

    }
    
    public function exportarOrdenPago($id){
        $orden_pago = OrdenPago::findOrFail($id);

        $monto_letras = NumeroALetras::convertir($orden_pago->MONTO, 'PESOS', 'CENTAVOS');
        
        $pdf = PDF::loadView('reportes.orden_pago', compact('orden_pago','monto_letras') );
        
        return $pdf->download('ORDEN_PAGO_'.time().'.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cuentass = Cuenta::orderBy('CODIGO','ASC')->get(); 

        foreach($cuentass as $cuenta){
            if($cuenta->perteneceAPresupuestoPago($cuenta)){
                $cuentas[] = $cuenta;
            }
        }

        $proveedores = Proveedor::all();
        $ordenes = OrdenPago::where('ID_SUPERIOR',0)->get();
        return view('orden_pago.create', compact('proveedores','cuentas','ordenes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            // echo OrdenPago::where('CODIGO', 11)->max('CODIGO_BIS') + 1;
            // die();

        request()->validate([
            'fecha'     => 'required',
            'monto'     => 'required',
            'proveedor' => 'required',
        ]);
        
        DB::beginTransaction();
        
        try {
            
            $filas  = count($request->cuentaid);
            $c      = 1;
            $pagado = 0;
            
            while($c < $filas){
                $pagado = $pagado + (int)$request->cancela[$c];
                $c++;
            }
            
            $orden_pago                 = new OrdenPago();
            $orden_pago->FECHA          = $request->fecha;
            $orden_pago->MONTO          = number_format((int)$request->monto,2,".","");
            $orden_pago->FK_PROVEEDOR   = $request->proveedor;
            
            $orden_pago->SALDO          = number_format((int)$request->monto,2,".",""); 
            $orden_pago->CONCEPTO       = $request->concepto;
            $orden_pago->NRO_COMP_RECI  = $request->comprobante; 
            $orden_pago->OBSERVACIONES  = $request->observaciones; 
            
            if($request->id_cuenta_superior != "null"){
                $orden_pago->ID_SUPERIOR        = $request->id_cuenta_superior;
                $orden_pago->CODIGO             = OrdenPago::findOrFail($orden_pago->ID_SUPERIOR)->CODIGO;
                $orden_pago->CODIGO_BIS         = OrdenPago::where('CODIGO', $orden_pago->CODIGO)->max('CODIGO_BIS') + 1;
            }else{
                $orden_pago->CODIGO         = (int) OrdenPago::max('CODIGO') + 1;
            }
                
            
            $orden_pago->CREATED_BY     = auth()->user()->name;
            $orden_pago->save();
            
            //recorrer filas de la tabla
            
            $c  = 1;
            while($c < $filas){
                
                $detalle_orden_pago                 = new DetalleOrdenPago();
                $detalle_orden_pago->FK_ORDEN_PAGO  = $orden_pago["id"];
                $detalle_orden_pago->FK_CUENTA      = $request->cuentaid[$c];
                $detalle_orden_pago->MONTO          = number_format((int) $request->imputa[$c],2,".","");
                $detalle_orden_pago->DEPARTAMENTO   = $request->departamento[$c];
                $detalle_orden_pago->CREATED_BY     = auth()->user()->name;
                $detalle_orden_pago->save();

                $mov_cuenta                     = new MovCuenta();
                $mov_cuenta->IS_INGRESO         = 0;
                $mov_cuenta->MONTO              = number_format((int) $request->imputa[$c],2,".","");
                $mov_cuenta->CUENTA_ORIGEN_ID   = NULL;
                $mov_cuenta->FK_CUENTA          = $request->cuentaid[$c];
                $mov_cuenta->OBSERVACIONES      = 'COD. ORDEN PAGO: ' . $orden_pago["codigo"];
                $mov_cuenta->DEPARTAMENTO       = $request->departamento[$c];
                $mov_cuenta->TIPO_MOV           = 'PAGO';
                $mov_cuenta->CREATED_BY         = auth()->user()->name;
                $mov_cuenta->UPDATED_BY         = '';
                $mov_cuenta->save();

                $this->actualizarSaldoCuenta($request->cuentaid[$c] , $request->imputa[$c], auth()->user()->name);
                
                $c++;
            }
            
            DB::commit();

            return redirect()->action(
                'OrdenPagoController@index', ['id' => $orden_pago["id"] ]
            );
            
        } catch (Exception $ex) {
            DB::rollBack();
            return redirect()->route('ordenes-de-pago.create')
                        ->with('danger','Error al crear la Orden de Pago. '. $ex->getMessage());
        }
        
    }

    private function actualizarSaldoCuenta($idCuenta, $monto, $usuario){

        $cuentaObj = Cuenta::findOrFail($idCuenta);

        while($cuentaObj->ID_CUENTA_SUPERIOR != NULL){
            
                $cuenta = Cuenta::where('ID', $cuentaObj->ID);
                $cuenta->update([
                        'SALDO'                 => number_format(($cuentaObj->SALDO + $monto), 2,".",""),
                        'MODIFICADO_POR'        => $usuario                
                ]);

            $cuentaObj = Cuenta::findOrFail($cuentaObj->ID_CUENTA_SUPERIOR);
        }

        $cuenta = Cuenta::where('ID', $cuentaObj->ID);
        $cuenta->update([
                'SALDO'                 => number_format(($cuentaObj->SALDO + $monto), 2,".",""),
                'MODIFICADO_POR'        => $usuario                
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
