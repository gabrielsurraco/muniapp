<?php

namespace App\Http\Controllers;
use App\Models\Proveedor;
use App\Http\Requests\ProveedorRequest;
use Maatwebsite\Excel\Concerns\FromCollection;
use Excel;
use Dompdf\Dompdf;
use PDF;
use App\Exports\ProveedoresExport;


class ProovedoresController extends Controller implements FromCollection
{
    public function collection()
    {
        return Proveedor::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proveedores = Proveedor::latest()->paginate(5);

        return view('proveedores.index',compact('proveedores'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('proveedores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Requests\ProveedorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProveedorRequest $request)
    {
        $proveedores = new Proveedor;     

        $proveedores -> codigo = $request->CODIGO;
        $proveedores -> nombre = $request->NOMBRE;
        $proveedores -> razon_social = $request->RAZON_SOCIAL;
        $proveedores -> cuil = $request->CUIL;
        $proveedores -> domicilio = $request->DOMICILIO;
        $proveedores -> telefono = $request->TELEFONO;
        $proveedores -> email = $request->EMAIL;
        $proveedores -> observaciones = $request->OBSERVACIONES;

        $proveedores->save();

        return redirect()->route('proveedores.index')
                            ->with('info','El proveedor fue guardado');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proveedor = Proveedor::find($id);
        return view('proveedores.show',compact('proveedor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proveedor = Proveedor::find($id);
         return view('proveedores.edit',compact('proveedor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Requests\ProveedorRequest  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProveedorRequest $request, $id)
    {
       
        Proveedor::where('ID', $id)
          ->update(['CODIGO' =>$request->CODIGO, 'NOMBRE' =>$request->NOMBRE, 'RAZON_SOCIAL' => $request->RAZON_SOCIAL ,'CUIL' =>  $request->CUIL,'DOMICILIO' => $request->DOMICILIO,'TELEFONO' => $request->TELEFONO,'EMAIL' => $request->EMAIL,'OBSERVACIONES' => $request->OBSERVACIONES]);

        return redirect()->route('proveedores.index')
                        ->with('success','Proveedor actualizado Correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
       
       $proveedor = Proveedor::where('ID', $id);
       $proveedor->delete();

        return redirect()->route('proveedores.index')
                        ->with('success','Proveedor Eliminado Correctamente!');
    }

    public function export(){
        return (new ProveedoresExport)->download('Proveedores'.time().'.xlsx');
    }

    public function exportarPDF(){
        
        $proveedores = Proveedor::orderBy('NOMBRE')->orderBy('RAZON_SOCIAL')->get();
        
        $pdf = PDF::loadView('proveedores.export.proveedores', compact('proveedores') );
        $pdf->setPaper('a4', 'landscape');
        return $pdf->download('PROVEEDORES_'.time().'.pdf');
    }

   
}
