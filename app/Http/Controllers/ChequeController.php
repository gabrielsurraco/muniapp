<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cheque;


class ChequeController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cheques = Cheque::latest()->paginate(5);

        return view('cheques.index',compact('cheques'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cheques.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cheques = new Cheque; 

        $cheques -> codigo = $request->CODIGO;
        $cheques -> numero_cuenta = $request->NUMERO_CUENTA;
        $cheques -> sucursal = $request->SUCURSAL;
        $cheques -> nombre = $request->NOMBRE;
        $cheques -> numero_inicial = $request->NUMERO_INICIAL;
        $cheques -> numero_final = $request->NUMERO_FINAL;
        

        $cheques->save();

        return redirect()->route('cheques.index')
                            ->with('info','El cheque fue guardado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cheque = Cheque::find($id);
        return view('cheques.show',compact('cheque'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cheque = Cheque::find($id);
         return view('cheques.edit',compact('cheque'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cheque::where('ID', $id)
          ->update(['CODIGO' =>$request->CODIGO,'NUMERO_CUENTA' =>$request->NUMERO_CUENTA, 'SUCURSAL' =>$request->SUCURSAL, 'NOMBRE' => $request->NOMBRE ,'NUMERO_INICIAL' =>  $request->NUMERO_INICIAL,'NUMERO_FINAL' => $request->NUMERO_FINAL]);
        return redirect()->route('cheques.index')
                       ->with('success','La chequera fue actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cheque = Cheque::where('ID', $id);
        $cheque->delete();

        return redirect()->route('cheques.index')
                        ->with('success','El cheque fue eliminado con exito');
    }
}
