<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cuenta;
use Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
//use Dompdf;
use Dompdf\Dompdf;






class HomeController extends Controller implements FromCollection
{
    
    public function collection()
    {
        return Cuenta::all();
    }
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function export(){
        return Excel::download(new HomeController, 'invoices.xlsx');
    }
    
    public function exportarPDF(){
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml('hello world');

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream();
    }
    
    
}
