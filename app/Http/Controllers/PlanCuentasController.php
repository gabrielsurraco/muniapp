<?php

namespace App\Http\Controllers;

use App\Models\Cuenta;
use Illuminate\Http\Request;

class PlanCuentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuentas = Cuenta::orderBy('ID_CUENTA_SUPERIOR','ASC')->orderBy('CODIGO','ASC')->get(); 

        return view('cuentas.index',compact('cuentas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $next_codigo = Cuenta::max('CODIGO') +1;
        
        
        $cuentas = Cuenta::all();
        return view('cuentas.create', compact("cuentas",'next_codigo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         request()->validate([
            'codigo' => 'required|unique:cuentas',
            'nombre' => 'required|unique:cuentas',
        ]);

        $cuenta                             = new Cuenta();
        $cuenta->CODIGO                     = $request->codigo;
        $cuenta->NOMBRE                     = $request->nombre;
        $cuenta->ID_CUENTA_SUPERIOR         = $request->id_cuenta_superior;
        $cuenta->OBSERVACIONES              = $request->observaciones;
        $cuenta->PRESUPUESTO                = $request->presupuesto;
        $cuenta->PRESUPUESTO_EJECUTIVO      = $request->presupuesto_ejecutivo;
        $cuenta->PRESUPUESTO_LEGISLATIVO    = $request->presupuesto_legislativo;
        $cuenta->IS_PRINCIPAL               = (isset($request->is_principal)) ? 1 : 0 ;
        $cuenta->CREADO_POR                 = auth()->user()->name;
        $cuenta->save();

        return redirect()->route('cuentas.index')
                        ->with('success','Cuenta Creada Correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
    public function show(Cuenta $cuenta)
    {
        if($cuenta->ID_CUENTA_SUPERIOR == NULL){
            $cuenta_superior_desc = "N/A";
        }else{
            $cuenta_superior = Cuenta::where('ID' , $cuenta->ID_CUENTA_SUPERIOR)->get();
            $cuenta_superior_desc = $cuenta_superior[0]->NOMBRE;
        }

        return view('cuentas.show',compact('cuenta','cuenta_superior_desc'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
    public function edit(Cuenta $cuenta)
    {
        $cuentas = Cuenta::all();
        
        
                
        return view('cuentas.edit',compact('cuenta','cuentas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request, Cuenta $cuenta)
//    {
//        
//        request()->validate([
//            'codigo' => 'required|unique:cuentas',
//            'nombre' => 'required|unique:cuentas',
//        ]);
//        
//
//        $cuenta->update($request->all());
//
//
//        return redirect()->route('cuentas.index')
//                        ->with('success','Cuenta Actualizada Correctamente');
//    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
    public function actualizarCuenta(Request $request,Cuenta $cuenta){
        request()->validate([
            'nombre' => 'required',
        ]);
        
        
        $cuenta = Cuenta::where('ID', $request->idcuenta);
        $cuenta->update([
                'NOMBRE'                    => $request->nombre, 
                'ID_CUENTA_SUPERIOR'        => $request->id_cuenta_superior, 
                'OBSERVACIONES'             => $request->observaciones,
                'PRESUPUESTO'               => $request->presupuesto,
                'PRESUPUESTO_EJECUTIVO'     => $request->presupuesto_ejecutivo,
                'PRESUPUESTO_LEGISLATIVO'   => $request->presupuesto_legislativo,
                'IS_PRINCIPAL'              => (isset($request->is_principal)) ? 1 : 0                
            ]);


        return redirect()->route('cuentas.index')
                        ->with('success','Cuenta Actualizada Correctamente');
    }


    public function eliminarCuenta($id){
        Cuenta::where('ID',$id)->delete();

        return redirect()->route('cuentas.index')
                        ->with('success','Cuenta Eliminada Correctamente!');
    }
}
