<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Models\Pago;
use App\Models\OrdenPago;
use App\Models\Cuenta;
use Illuminate\Support\Facades\DB;
use App\Models\Banco;
use App\Models\Chequera;
use App\Models\Cheque;
use App\Models\Proveedor;

class PagosController extends Controller
{

    public function payOrder($id){
        $order      = OrdenPago::findOrFail($id);
        $bancos     = Banco::all();
        $chequeras  = Chequera::all();

        return view('pagos.create', compact('order','bancos','chequeras'));
    }

    public function savePayment(Request $request ){

        $filas  = count($request->monto);
        $c      = 1;
        $pagado = 0;
        
        while($c < $filas){
            $pagado = $pagado + (int)$request->monto[$c];
            $c++;
        }

        DB::beginTransaction();
        
        try{
            $saldo_anterior     = (int) OrdenPago::findOrFail($request->orden_id)->SALDO;
            $saldo_actualizado  = number_format(($saldo_anterior - $pagado) ,2,".","");

            $order = OrdenPago::where('ID',$request->orden_id)->update([
                'SALDO'         => $saldo_actualizado,
                'UPDATED_BY'    => auth()->user()->name
            ]);

            $c      = 1;
            
            while($c < $filas){

                if($request->m_pago[$c] == 'cheque'){
                    //creo cheque
                    $banco = 0;
                    $cheque     = Cheque::where('NUMERO_CHEQUE', $request->nro_cheque[$c])->where('FK_CHEQUERA', $request->id_chequera[$c])->get();
                    $chequera   = Chequera::where('ID', $request->id_chequera[$c])->first();
                    $min        = $chequera->DESDE;
                    $max        = $chequera->HASTA;

                    if(count($cheque) == 0){
                        if(($request->nro_cheque[$c] >= $min) && ($request->nro_cheque[$c] <= $max)){
                            
                            
                            $newCheque                 = new Cheque();
                            $newCheque->NUMERO_CHEQUE  = (int) $request->nro_cheque[$c];
                            $newCheque->FK_CHEQUERA    = (int ) $request->id_chequera[$c];
                            $newCheque->FECHA_EMISION  = $request->fecha[$c];
                            $newCheque->IMPORTE        = number_format((int) $request->monto[$c],2,".","");
                            $newCheque->CREATED_BY     = auth()->user()->name;
                            $newCheque->save();

                        }
                    }

                }else{
                    $cheque     = 0;
                    $banco      = $request->c_banco[$c];
                }
                
                $pago                   = new Pago();
                $pago->FK_ORDEN_PAGO    = $request->orden_id;
                $pago->FECHA            = $request->fecha[$c];
                $pago->MONTO            = number_format((int) $request->monto[$c],2,".","");
                $pago->TIPO_PAGO        = $request->m_pago[$c];
                
                if($request->m_pago[$c] == 'cheque'){
                    $pago->FK_CHEQUE        = $newCheque["id"];
                }else{
                    $pago->FK_BANCO         = $banco;
                }
                
                $pago->CREATED_BY       = auth()->user()->name;
                $pago->save();

                $c++;
            }

            DB::commit();
            return redirect()->route('pagos.index')->with('success' , 'Se realizo el pago correctamente!' );
        }catch(Exception $ex){
            DB::rollBack();
            dd($ex->getMessage());
            //return redirect()->back()->with('danger' , 'Error al guardar el pago' . $ex->getMessage() );
        }     

    }

    public function index(){
        $pagos          = Pago::paginate(10);
        $proveedores    = Proveedor::all();
        return view('pagos.index',compact('pagos','proveedores'));
    }

    public function verificarDisponibilidadCheque(Request $request){
        if($request->ajax()){

            if(isset($request->chequera) && isset($request->cheque)){

                $cheque = Cheque::where('NUMERO_CHEQUE', $request->cheque)->where('FK_CHEQUERA', $request->chequera)->get();

                if(count($cheque) > 0){
                    return Response::json([
                        'existe'    => false,
                        'mensaje'   => 'Dicho número de cheque ya fue utilizado'
                    ], 200);
                }else{
                    $chequera   = Chequera::where('ID', $request->chequera)->first();
                    $min        = $chequera->DESDE;
                    $max        = $chequera->HASTA;
                    
                    if(($request->cheque >= $min) && ($request->cheque <= $max)){
                        return Response::json([
                            'existe'    => true,
                            'mensaje'   => 'Dicho número de cheque es válido'
                        ], 200);
                    }else{
                        return Response::json([
                            'existe'    => false,
                            'mensaje'   => 'El número no se encuentra en el rango de la chequera'
                        ], 200);
                    }
                    
                }

            }else{
                return Response::json([
                    'existe'    => false,
                    'mensaje'   => 'Favor validar campos del cheque'
                ], 200);
            }

            
        }
    }

}
