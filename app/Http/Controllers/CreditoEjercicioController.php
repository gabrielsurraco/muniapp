<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cuenta;
use App\Models\OrdenPago;
use App\Models\Proveedor;
use App\Models\MovCuenta;
use Illuminate\Support\Facades\DB;

class CreditoEjercicioController extends Controller
{
     public function collection()
    {
        return MovCuenta::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movcuentas = MovCuenta::latest()->paginate(5);

        return view('creditos_ejercicio.index',compact('movcuentas'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $movcuentas = MovCuenta::all();
    	$cuentas = Cuenta::all();
        return view('creditos_ejercicio.create', compact('cuentas'));
    }

    public function presupuesto()
    {
        $movcuentas = MovCuenta::all();
    	$cuentas = Cuenta::all();
        return view('creditos_ejercicio.presupuesto', compact('cuentas','movcuentas'));
    }

     public function compensacion()
    {
        $movcuentas = MovCuenta::all();
    	$cuentas = Cuenta::all();
        return view('creditos_ejercicio.compensacion', compact('cuentas','movcuentas'));
    }



     public function store(Request $request)
    {
        DB::beginTransaction();

        try {

            $movcuenta = new MovCuenta();
            $movcuenta->FECHA = $request->fecha;

         	if ($request->fk_cuenta != null) {
         		$movcuenta->FK_CUENTA = $request->fk_cuenta;
                $movcuenta->FK_CUENTA_DEBITAR = 0;
         	}else{
                $movcuenta->FK_CUENTA = $request->fk_acreditar;
                $movcuenta->FK_CUENTA_DEBITAR = $request->fk_debitar;
            }

            $movcuenta->OBSERVACIONES = $request->observaciones;
            $movcuenta->IS_INGRESO = "1";

            if ($request->presupuesto == "4") {
            	$movcuenta->TIPO_MOV = "PRESUPUESTO_INICIAL";
            }elseif ($request->compensacion == "5") {
            	$movcuenta->TIPO_MOV = "COMPENSACION";
            }else{
            	$movcuenta->TIPO_MOV = "MODIF_PRESUPUESTO";
            }

            if ($movcuenta->FK_CUENTA < 1000) {
                $movcuenta->DEPARTAMENTO = null;
            }elseif ($movcuenta->FK_CUENTA >= 1000 && $movcuenta->FK_CUENTA < 2000) {
                $movcuenta->DEPARTAMENTO = "EJECUTIVO";
            }elseif ($movcuenta->FK_CUENTA >= 2000) {
                $movcuenta->DEPARTAMENTO = "LESGISLATIVO";
            }

            $movcuenta->MONTO = $request->monto;

            //PRESUPUESTO
            if ($movcuenta->DEPARTAMENTO == "EJECUTIVO" && $movcuenta->rcuenta["PRESUPUESTO_EJECUTIVO"] == "0") {

                Cuenta::where("ID", $request->fk_cuenta)->update(["PRESUPUESTO_EJECUTIVO" => $request->monto]  ); 
                $movcuenta->save();

            }elseif ($movcuenta->DEPARTAMENTO == "LESGISLATIVO" && $movcuenta->rcuenta["PRESUPUESTO_LEGISLATIVO"] == "0") {

                Cuenta::where("ID", $request->fk_cuenta)->update(["PRESUPUESTO_LEGISLATIVO" => $request->monto ]  ); 

                $movcuenta->save();

            }elseif ($movcuenta->DEPARTAMENTO == "" && $movcuenta->rcuenta["PRESUPUESTO"] == "0") {

                Cuenta::where("ID", $movcuenta->FK_CUENTA)->update(["PRESUPUESTO" => $request->monto + $movcuenta->rcuenta["PRESUPUESTO"] ]   );

                do{
                    $cuenta_superior = new MovCuenta();
                    $cuenta_superior->FK_CUENTA = $movcuenta->rcuenta["ID_CUENTA_SUPERIOR"];
                    $movcuenta->rcuenta["ID_CUENTA_SUPERIOR"] = $cuenta_superior->rcuenta["ID_CUENTA_SUPERIOR"];
                    Cuenta::where("ID", $cuenta_superior->FK_CUENTA)->update(["PRESUPUESTO" => $request->monto + $movcuenta->rcuenta["PRESUPUESTO"] ]   );
                    echo $cuenta_superior->FK_CUENTA;
                }while ($cuenta_superior->FK_CUENTA != null);
                 

                $movcuenta->save();
                
            }
  			
      			
      			

      			//AUMENTO PRESUPUESTARIO
      			if ($request->departamento == "1" && $movcuenta->rcuenta["PRESUPUESTO_EJECUTIVO"] > "0" ) {

      				Cuenta::where("ID", $request->fk_cuenta)->update(["PRESUPUESTO_EJECUTIVO" => $request->monto + $movcuenta->rcuenta["PRESUPUESTO_EJECUTIVO"]]  ); 	

      			}elseif ($request->departamento == "2" && $movcuenta->rcuenta["PRESUPUESTO_LEGISLATIVO"] > "0" ) {

      				Cuenta::where("ID", $request->fk_cuenta)->update(["PRESUPUESTO_LEGISLATIVO" => $request->monto + $movcuenta->rcuenta["PRESUPUESTO_LEGISLATIVO"] ]  ); 
      			}

      			//COMPENSACION

      			if ($request->departamento == "1" && $request->compensacion == "5") {
      				Cuenta::where("ID", $request->fk_debitar)->update(["PRESUPUESTO_EJECUTIVO" => $movcuenta->rcuenta_debitar["PRESUPUESTO_EJECUTIVO"] - $request->monto ]);
                        //dd( $movcuenta->rcuenta_debitar["PRESUPUESTO_EJECUTIVO"]);
      				Cuenta::where("ID", $request->fk_acreditar)->update(["PRESUPUESTO_EJECUTIVO" => $request->monto + $movcuenta->rcuenta["PRESUPUESTO_EJECUTIVO"] ]);
      			}
  			
            DB::commit();
            if ($request->presupuesto == "4") {
              return redirect()->route('creditos-del-ejercicio.presupuesto')
                            ->with('success','Presupuesto creado correctamente.');    
            }else{
              return redirect()->route('creditos-del-ejercicio.index')
                            ->with('success','Presupuesto creado correctamente.');
            }
            
        } catch (Exception $ex) {
            DB::rollBack();
            return redirect()->route('creditos_ejercicio.presupuesto')
                            ->with('danger','Error al crear la orden de Pago.');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
