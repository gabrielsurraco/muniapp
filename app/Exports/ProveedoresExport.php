<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Proveedor;
//use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class ProveedoresExport implements FromView//, FromQuery
{
    use Exportable;
    public function view(): View
    {
        return view('proveedores.export.proveedores', [
            'proveedores' => Proveedor::orderBy('NOMBRE')->orderBy('RAZON_SOCIAL')->get()
        ]);
    }
    
     /*public function query()
    {
        return Invoice::query();
    }*/
}