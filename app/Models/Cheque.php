<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DateTime;

class Cheque extends Model
{
    protected $table = "CHEQUES";
    protected $guarded = [];

    public function getFechaEmisionAttribute( $value ) {
        return (new Carbon($value))->format('d/m/Y');
    }

    public function setFechaEmisionAttribute( $value ) {
        $this->attributes['FECHA_EMISION'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function chequera(){
        return $this->belongsTo('App\Models\Chequera','FK_CHEQUERA');
    }
}
