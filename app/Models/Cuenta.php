<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MovCuenta;

class Cuenta extends Model
{
    protected $table = 'CUENTAS';
//    protected $guarded = ["ID"];
    
    protected $fillable = [
        'codigo', 'nombre','id_cuenta_superior','observaciones','PRESUPUESTO','PRESUPUESTO_EJECUTIVO','PRESUPUESTO_LEGISLATIVO','saldo','is_principal','creado_por','modificado_por'
    ];

    public function nombreCuentaSuperior()
    {
        
        return $this->belongsTo('App\Models\Cuenta','ID_CUENTA_SUPERIOR');
        
    }

    public function cuentasuperior(){
        return $this->belongsTo('App\Models\Cuenta','ID_CUENTA_SUPERIOR');
    }

    public function cuentamov()
    {
        return $this->hasOne('App\Models\MovCuenta','FK_CUENTA');
        //return $this->hasOne('App\Models\MovCuenta','FK_CUENTA_DEBITAR');
    }

     public function cuentamovdebitar()
    {
        return $this->hasOne('App\Models\MovCuenta','FK_CUENTA_DEBITAR');
    }

    public function perteneceAPresupuestoPago($cuenta){

        $cuentaObj = Cuenta::findOrFail($cuenta->ID);

        if($cuentaObj->ID_CUENTA_SUPERIOR != NULL){

            while($cuentaObj->ID_CUENTA_SUPERIOR != NULL){     
                $cuentaObj = Cuenta::findOrFail($cuentaObj->ID_CUENTA_SUPERIOR);
            }
            
            if($cuentaObj->ID == 149)
                return true;
            else
                return false;

        }else{

            if($cuentaObj->ID == 149)
                return true;
            else
                return false;

        }
        
    }

}
