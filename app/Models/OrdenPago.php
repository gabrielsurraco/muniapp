<?php

namespace App\Models;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class OrdenPago extends Model
{
    protected $table = 'ordenes_de_pago';
    protected $guarded = [];
    
    public function getFechaAttribute( $value ) {
        return (new Carbon($value))->format('d/m/Y');
    }

    public function setFechaAttribute( $value ) {
        $this->attributes['FECHA'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function proveedor(){
        return $this->belongsTo('App\Models\Proveedor','FK_PROVEEDOR');
    }

    public function detalles(){
        return $this->hasMany('App\Models\DetalleOrdenPago','FK_ORDEN_PAGO','ID');
    }

    

}
