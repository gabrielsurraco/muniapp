<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleOrdenPago extends Model
{
    protected $table    = "DETALLES_ORDENES_DE_PAGO";
    protected $guarded  = [];

    public function cuenta(){
        return $this->belongsTo('App\Models\Cuenta','FK_CUENTA');
    }

    
    
}
