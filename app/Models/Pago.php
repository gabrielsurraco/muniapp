<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Pago extends Model
{
    protected $table = 'PAGOS';
    protected $guarded = [];
    
    public function getFechaAttribute( $value ) {
        return (new Carbon($value))->format('d/m/Y');
    }

    public function setFechaAttribute( $value ) {
        $this->attributes['FECHA'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function ordenpago(){
        return $this->belongsTo('App\Models\OrdenPago','FK_ORDEN_PAGO');
    }

    public function banco(){
        return $this->belongsTo('App\Models\Banco','FK_BANCO','ID');
    }

    public function cheque(){
        return $this->belongsTo('App\Models\Cheque','FK_CHEQUE','ID');
    }

}
