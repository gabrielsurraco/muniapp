<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cuenta;

class MovCuenta extends Model
{
    protected $table = "MOV_DE_CUENTAS";

    protected $fillable = [
        'IS_INGRESO','FECHA', 'MONTO','CUENTA_ORIGEN_ID','FK_CUENTA','FK_CUENTA_DEBITAR','OBSERVACIONES','DEPARTAMENTO','TIPO_MOV','CREATED_BY','UPDATED_BY'
    ];
    
    //Relacón
    public function rcuenta(){
    	return $this->belongsTo('App\Models\Cuenta','FK_CUENTA');
    }

    /*public function rcuenta()
    {
        
        return $this->hasOne('App\Models\Cuenta','FK_CUENTA');
        
    }*/
}
