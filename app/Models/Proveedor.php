<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = "PROVEEDORES";
    
    protected $fillable = [
        'CODIGO', 'NOMBRE', 'RAZON_SOCIAL','CUIL','DOMICILIO','TELEFONO','EMAIL','OBSERVACIONES'
    ];
    
    
    
}
