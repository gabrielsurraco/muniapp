@extends('layouts.backend')


@section('content')
<div class="wrapper">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">Registro de Pagos</div>
            <div class="ibox-content">
            <form action="{{  route('pagos.guardar')  }}" method="POST">
                    @csrf
                <div class="row">
                    <div class="col-md-4">
                            <div style="border: 1px dashed black; padding: 10px; overflow: hidden;">
                                <div class="col-md-12">
                                    <label>Concepto:</label> {{ $order->CONCEPTO }} ({{ 'ORDEN DE PAGO NRO. ' . $order->CODIGO }})
                                </div>
                                <div class="col-md-12">
                                    <label>Proveedor:</label> {{ $order->proveedor->NOMBRE }} ({{ $order->proveedor->RAZON_SOCIAL }}) - {{ $order->proveedor->CUIL }}
                                </div>
                                <div class="col-md-12">
                                    <label>Valor de la Orden de Pago:</label> $ {{ $order->MONTO }}
                                </div>
                                <div class="col-md-12">
                                    <label>Saldo Pendiente:</label> $ {{ $order->SALDO }}
                                </div>
                                <div class="col-md-12">
                                    <label>Observaciones:</label> {{ $order->OBSERVACIONES }}
                                </div>
                                <input type="hidden" name="orden_id" value="{{ $order->ID }}" />
                            </div>
                            <div class="col-md-12 text-center m-t-md">
                                    <h1 class="total">TOTAL: $ <span class="pago_total">0.00</span></h1>
                            </div>
                            <div class="col-md-12 text-center m-t-md">
                                <button class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Guardar</button>
                                <a class="btn btn-default"> <i class="fa fa-step-backward"></i> Cancelar</a>
                            </div>
                    </div>
                    <div class="col-md-7">
                        <h2 style="border-bottom: 1px solid #2f4050; padding-bottom: 5px;">Carga de Pagos</h2>
                        <a class="btn btn-warning btn-md " data-toggle='modal' data-target='#myModal6'> <i class="fa fa-money"></i> &nbsp;  Agregar Pago</a>
                        <table class="table table-bordered m-t-sm">
                            <thead>
                                <tr>
                                    <td>Fecha</td>
                                    <td>Método de Pago</td>
                                    <td>Monto</td>
                                    <td>Cuenta</td>
                                    <td>Cheque</td>
                                    <td>Acción</td>
                                </tr>
                            </thead>
                            <tbody class="cuerpo-tabla">

                                <tr style="display: none;" class="modelo">
                                    <td style="padding: 4px !important;"> <input type="hidden" value="0" name="fecha[]"  /> <span></span></td>
                                    <td style="padding: 4px !important;"> <input type="hidden" value="0" name="m_pago[]"  /> <span></span> </td>
                                    <td style="padding: 4px !important;"> <input type="hidden" value="0" name="monto[]"   /> <span></span></td>
                                    <td style="padding: 4px !important;"> <input type="hidden" value="0" name="c_banco[]" /> <span></span></td>
                                    <td style="padding: 4px !important;"> 
                                        <input type="hidden" value="0" name="id_chequera[]" />
                                        <input type="hidden" value="0" name="nro_cheque[]" />
                                        <span></span>
                                    </td>
                                    <td style="padding: 4px !important;">
                                            <a class="eliminar-pago"><button title='Eliminar' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i></button></a>
                                    </td>
                                    
                                </tr>

                            </tbody>
                        
                        </table>
                    </div>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>



<div class="modal inmodal fade" id="myModal6"  role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Seleccione el medio de Pago</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Fecha:</strong>
                                <input name="fecha_pago" type="text" class="form-control fecha" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Método de Pago:</strong>
                                <select type="text" name="metodo_pago" class="form-control" placeholder="Metodo de Pago">
                                    <option value="null" tabindex="-1">Seleccione el método de pago...</option>
                                    <option value="transferencia">Transferencia</option>
                                    <option value="cheque">Cheque</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 banco">
                            <div class="form-group">
                                <strong>Cuenta:</strong>
                                <select type="text" name="cuenta" class="form-control" placeholder="Cuenta">
                                    <option value="null" tabindex="-1">Seleccione la cuenta...</option>
                                    @foreach($bancos as $banco)
                                        <option value="{{ $banco->ID }}">{{ $banco->NOMBRE }} - {{ $banco->NUMERO_CUENTA }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12 chequera" style="display: none;">
                            <div class="form-group form-inline">
                                
                                <select type="text" style="width: 47%;" name="chequera" class="form-control" placeholder="Chequera">
                                    <option value="null" tabindex="-1">Seleccione Chequera...</option>
                                    @foreach($chequeras as $chequera)
                                        <option value="{{ $chequera->ID }}">{{ $chequera->NUMERO_CHEQUERA }} - {{ $chequera->NOMBRE }}</option>
                                    @endforeach
                                    
                                </select>
                                -
                                <input name="cheque" placeholder="Nro. de Cheque" type="text"  class="form-control">
                                <a class="btn btn-primary verificar-cheque"> <i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                                <div class="form-group">
                                        <strong>Monto a pagar:</strong>
                                        <input name="monto" type="text" placeholder="0.00" class="form-control dinero">
                                </div>  
                        </div>
                        
                    </div>
                    <div class="row">
                        <div style="text-align: center;" class="m-t-md">
                            <span class="alert alert-success" style="display: none">El número de cheque esta Disponible.</span>
                            <span class="alert alert-danger" style="display: none">El número de cheque NO esta Disponible.</span>
                        </div>
                        <input type="hidden" name="estado_cheque" value="0" />
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary boton-agregar">Agregar</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    <script>
        $(function(){
            var pago_total = 0;

            $( ".fecha" ).datepicker({
                format: 'dd/mm/yyyy',
            });
            
            $(".fecha").datepicker("setDate", "0");

            $('.nuevo-pago').click(function(){
                $('a.nuevo').attr('data-toggle','modal');
                $('a.nuevo').attr('data-target','#myModal6');
            });

            $(".dinero").on("input", function() {
                // allow numbers, a comma or a dot
                var v= $(this).val(), vc = v.replace(/[^0-9\.]/, '');
                if (v !== vc)        
                    $(this).val(vc);
            });

            $("select[name='metodo_pago']").change(function(){
                if(this.value == 'transferencia'){
                    $('.banco').show();
                    $('.chequera').hide();
                    $("input[name='estado_cheque']").val(1);
                }else{
                    $('.banco').hide();
                    $('.chequera').show();
                }
            });

            $('.boton-agregar').click(function(e){
                e.preventDefault();
                var metodo_pago = $("select[name='metodo_pago'] option:selected").val();
                var monto       = $("input[name='monto']").val();

                if($("input[name='fecha_pago']").val() == ''){
                    alert('Favor ingrese la fecha de pago');
                    return false;
                }

                if(monto == 'undefined' || monto == '' || monto == 0){
                    alert('Favor ingrese un monto válido');
                    return false;
                }

                if($("input[name='estado_cheque']").val() == 0){
                    alert('Favor verifique el Nro de Cheque');
                    return false;
                }

                
                
                if(metodo_pago != 'null'){

                    if(metodo_pago == 'transferencia'){
                        
                        var idCuenta = $("select[name='cuenta'] option:selected").val();
                        var monto       = $("input[name='monto']").val();

                        if(idCuenta == 'null'){
                            alert('Favor seleccione una Cuentass');
                            return false;
                        }
                        
                        var tr = $('.modelo').clone(true);

                        tr.find('td:eq(0) span').text($("input[name='fecha_pago']").val());
                        tr.find("td:eq(0) input").val($("input[name='fecha_pago']").val());
                        tr.find('td:eq(1) span').text('TRANSFERENCIA');
                        tr.find("td:eq(1) input").val(metodo_pago);
                        tr.find('td:eq(2) span').text('$ ' + monto);
                        tr.find("td:eq(2) input").val(monto);
                        tr.find('td:eq(3) span').text($("select[name='cuenta'] option:selected").text());
                        tr.find("td:eq(3) input").val(idCuenta);
                        
                        tr.show();
                        tr.removeClass('modelo');
                        $('.cuerpo-tabla').append(tr);
                        

                    }else{

                        var chequera    = $("select[name='chequera'] option:selected").val();
                        var cheque      = $("input[name='cheque']").val();

                        if(cheque == '' || cheque =='undefined'){
                            alert('Favor ingrese un Nro. de Cheque');
                            return false;
                        }

                        if(chequera == 'null'){
                            alert('Favor seleccione una chequera');
                            return false;
                        }

                        var tr = $('.modelo').clone(true);
                        
                        tr.find('td:eq(0) span').text($("input[name='fecha_pago']").val());
                        tr.find("td:eq(0) input").val($("input[name='fecha_pago']").val());
                        tr.find('td:eq(1) span').text('CHEQUE');
                        tr.find("td:eq(1) input").val(metodo_pago);
                        tr.find('td:eq(2) span').text('$ ' + monto);
                        tr.find("td:eq(2) input").val(monto);
                        var desc = $("select[name='chequera'] option:selected").text() + ' - ' + $("input[name='cheque']").val();
                        tr.find('td:eq(4) span').text(desc);
                        tr.find("td:eq(4) input[name='id_chequera[]']").val(chequera);
                        tr.find("td:eq(4) input[name='nro_cheque[]']").val(cheque);

                        tr.show();
                        tr.removeClass('modelo');
                        $('.cuerpo-tabla').append(tr);

                    }
                }else{

                    alert('Favor seleccione el método de Pago');
                    return false;

                }

                pago_total = pago_total + parseInt(monto);

                $('.pago_total').text(pago_total.toFixed(2));

                $('#myModal6').modal('toggle');

                $("select[name='cuenta'] option:eq(0)").prop('selected', true);
                $("input[name='monto']").val("");
                $("select[name='metodo_pago'] option:eq(0)").prop('selected', true);
                $("select[name='chequera'] option:eq(0)").prop('selected', true);
                $("input[name='cheque']").val("");
                $("input[name='estado_cheque']").val(0);
                $(".fecha").datepicker("setDate", "0");
                


            });

            $('.eliminar-pago').click(function(){
                var montoArestar = $(this).parent().parent().find("td:eq(2) input").attr('value');
                pago_total = pago_total - parseInt(montoArestar);

                $('.pago_total').text(pago_total.toFixed(2));

                $(this).parent().parent().remove();

            });

            $('.verificar-cheque').click(function(){
                var chequera    = $("select[name='chequera'] option:selected").val();
                var cheque      = $("input[name='cheque']").val();

                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    datatType : 'json',
                    method: "POST",
                    url: "/pagos/disponibilidad-cheque",
                    data: { chequera: chequera, cheque: cheque, _token: "{{ csrf_token() }}" }
                }).done(function(m){

                    if(m.existe){
                        $(".alert-success").text(m.mensaje);
                        $(".alert-success").show().delay(2000).hide(0);
                        $(".alert-danger").hide();

                        $("input[name='estado_cheque']").val(1);
                    }else{
                        $(".alert-danger").text(m.mensaje);
                        $(".alert-success").hide();
                        $(".alert-danger").show().delay(2000).hide(0);
                        $("input[name='estado_cheque']").val(0);
                    }
                    
                }).fail(function(m){
                    $(".alert-danger").text(m.mensaje);
                    $(".alert-success").hide();
                    $(".alert-danger").show().delay(2000).hide(0);
                });
            });

            
            
            

        });
    </script>
@stop
