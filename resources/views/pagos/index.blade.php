@extends('layouts.backend')

@section('css')

    <link href="/backend/css/select2/select2.min.css" rel="stylesheet" />
    <link href="/backend/css/select2/select2-bootstrap.css" rel="stylesheet" />
    <style>
        ul.pagination{
            margin-top: 0px !important;
        }
    </style>
@stop

@section('content')
    <div class="wrapper">
        <div class="row">
            <div class="ibox float-e-margins">
                <div class="ibox-title">Listado de Pagos realizados</div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                        </div>
                    </div>
                    <div class="row wrapper">
                        
                            
                            
                            <div class="col-md-2 form-group">
                                    <strong>Fecha Desde:</strong>
                                    <input name="fecha_desde" placeholder="Fecha Desde" type="text" class="form-control fecha" />
                            </div>
                            <div class="col-md-2 form-group">
                                    <strong>Fecha Hasta:</strong>
                                    <input name="fecha_hasta" placeholder="Fecha Hasta" type="text" class="form-control fecha" />
                            </div>

                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <strong>Proveedor:</strong>
                                    
                                    <select  name="proveedor" class="form-control select2" placeholder="Cuenta Superior" style="width: 100%;">
                                        <option value="null" tabindex="-1">Seleccione una opción...</option>
                                        @foreach($proveedores as $pro)
                                            <option value="<?php echo $pro->ID; ?>">{{ $pro->CODIGO ." - ". $pro->NOMBRE." - ". $pro->RAZON_SOCIAL }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="col-xs-2 col-sm-2 col-md-2">
                                <div class="form-group">
                                    <strong>Medio de Pago:</strong>
                                    
                                    <select  name="medio_pago" class="form-control select2" style="width: 100%;">
                                        <option value="null" tabindex="-1">Medio de Pago...</option>
                                        <option value="cheque">CHEQUE</option>
                                        <option value="transferencia">TRANSFERENCIA</option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                    <strong>Nro. Orden de Pago:</strong>
                                    <input name="nro_orden_pago" placeholder="Nro. Orden Pago" type="text" class="form-control" />
                            </div>

                        
                        <div class="col-md-12">
                            <a href="#" class="btn btn-md btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                            <a href="#" class="btn btn-md btn-default"> <i class="fa fa-backward"></i> Cancelar</a>
                        </div>
                    </div>
                    <div class="row m-t-md">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td style="font-weight: bold;">Fecha Pago</td>
                                        <td style="font-weight: bold;">Nro. Orden Pago</td>
                                        <td style="font-weight: bold;">Proveedor</td>
                                        <td style="font-weight: bold;">Medio Pago</td>
                                        <td style="font-weight: bold;">Monto</td>
                                        <td style="font-weight: bold;">Cuenta Bancaria</td>
                                        <td style="font-weight: bold;">Cheque</td>
                                        <td style="font-weight: bold;">Acción</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pagos as $pago)
                                    <tr>
                                        <td>{{ $pago->FECHA }}</td>
                                        <td>{{ $pago->ordenpago->CODIGO }}</td>
                                        <td>{{ $pago->ordenpago->proveedor->NOMBRE }} - {{ $pago->ordenpago->proveedor->RAZON_SOCIAL }} - {{ $pago->ordenpago->proveedor->CUIL }}</td>
                                        <td>
                                            @if($pago->TIPO_PAGO == 'TRANSFERENCIA')
                                                <small class="label label-success">{{ $pago->TIPO_PAGO }} </small>
                                            @else
                                                <small class="label label-primary">{{ $pago->TIPO_PAGO }} </small>
                                            @endif
                                        </td>
                                        <td>{{ $pago->MONTO }}</td>
                                        <td> @if($pago->FK_BANCO > 0) {{ $pago->banco->NOMBRE }} - {{ $pago->banco->NUMERO_CUENTA }} @endif</td>
                                        <td>@if($pago->FK_CHEQUE > 0) {{ $pago->cheque->chequera->NUMERO_CHEQUERA }} {{ $pago->cheque->chequera->NOMBRE }} - {{ $pago->cheque->NUMERO_CHEQUE }} @endif</td>
                                        <td>
                                            <a class="btn btn-xs btn-warning"><i class="fa fa-ticket"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 text-center">
                            {{ $pagos->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script src="/backend/js/plugins/select2/select2.full.min.js"></script>

<script>
        $(function(){
            var pago_total = 0;

            $( ".fecha" ).datepicker({
                format: 'dd/mm/yyyy',
            });
            
            //$(".fecha").datepicker("setDate", "0");
            
            $.fn.select2.defaults.set( "theme", "bootstrap" );
            $('.select2').select2({
                placeholder: 'Select an option',                
            });
        });
</script>
@stop
