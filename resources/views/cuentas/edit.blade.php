@extends('layouts.backend')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Cuentas</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hay algunos inconvenientes en el formulario.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    <form action="{{ route('actualizarcuenta') }}" method="POST">
                        @csrf
                        <input name="idcuenta" value="{{$cuenta->ID}}" type="hidden">
                         <div class="row wrapper">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Código:</strong>
                                    <input value="{{ $cuenta->CODIGO }}" type="text" name="codigo" class="form-control" placeholder="Código" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Nombre:</strong>
                                    <input value="{{ $cuenta->NOMBRE }}" type="text" name="nombre" class="form-control" placeholder="Nombre">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Cuenta Superior:</strong>
                                    <select type="text" name="id_cuenta_superior" class="form-control" placeholder="Cuenta Superior">
                                        @foreach($cuentas as $c)
                                            @if($c->ID == $cuenta->ID_CUENTA_SUPERIOR)
                                                <option value="{{ $c->ID }}" selected="">{{ $c->CODIGO . " - " . $c->NOMBRE }}</option>
                                            @else
                                                <option value="{{ $c->ID }}" >{{ $c->CODIGO . " - " . $c->NOMBRE  }}</option>
                                            @endif
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Observaciones:</strong>
                                    <textarea name="observaciones" class="form-control" placeholder="Observaciones...">{{ $cuenta->OBSERVACIONES }}</textarea>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>PRESUPUESTO:</strong>
                                        <input type="number" name="presupuesto" value="{{ $cuenta->PRESUPUESTO }}" class="form-control" placeholder="$ 0">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>PRESUPUESTO EJECUTIVO:</strong>
                                            <input type="number" value="{{ $cuenta->PRESUPUESTO_EJECUTIVO }}" name="presupuesto_ejecutivo" class="form-control" placeholder="$ 0">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>PRESUPUESTO LEGISLATIVO:</strong>
                                                <input type="number" value="{{ $cuenta->PRESUPUESTO_LEGISLATIVO }}" name="presupuesto_legislativo" class="form-control" placeholder="$ 0">
                                            </div>
                                    </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Cuenta Principal:</strong>
                                    @if($cuenta->IS_PRINCIPAL == 1)
                                        <input type="checkbox" name="is_principal" class="form-control" checked="">
                                    @else
                                        <input type="checkbox" name="is_principal" class="form-control" >
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <a class="btn btn-default" href="{{ route('cuentas.index') }}"> Regresar</a>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>








    
    
    

@endsection