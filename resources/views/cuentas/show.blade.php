@extends('layouts.backend')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Cuentas</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    
                    
                    <form action="#" method="POST">
                        @csrf

                         <div class="row wrapper">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Código:</strong>
                                    <input value="{{ $cuenta->CODIGO }}" type="text" name="codigo" class="form-control" placeholder="Código" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Nombre:</strong>
                                    <input value="{{ $cuenta->NOMBRE }}" type="text" name="nombre" class="form-control" placeholder="Nombre" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Cuenta Superior:</strong>
                                    <input value="{{ $cuenta_superior_desc }}" type="text" name="id_cuenta_superior" class="form-control" placeholder="Nombre" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Observaciones:</strong>
                                    <textarea name="observaciones" class="form-control" placeholder="Observaciones..." readonly="">{{ $cuenta->OBSERVACIONES }}</textarea>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Saldo:</strong>
                                    <input type="number" value="{{ $cuenta->SALDO }}" name="saldo" class="form-control" placeholder="$ 0" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Cuenta Principal:</strong>
                                    @if($cuenta->IS_PRINCIPAL)
                                    <input type="checkbox" name="is_principal" class="form-control" checked="" readonly="">
                                    @else
                                    <input type="checkbox" name="is_principal" class="form-control"  readonly="">
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <a class="btn btn-default" href="{{ route('cuentas.index') }}"> Regresar</a>
                            </div>
                        </div>


                    </form>
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>








    
    
    

@endsection