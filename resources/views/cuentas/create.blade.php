@extends('layouts.backend')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Cuentas</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hay algunos inconvenientes en el formulario.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    <form action="{{ route('cuentas.store') }}" method="POST">
                        @csrf

                         <div class="row wrapper">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Código:</strong>
                                    <input type="text" name="codigo" value="{{ $next_codigo }}" class="form-control" placeholder="Código">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Nombre:</strong>
                                    <input type="text" name="nombre" class="form-control" placeholder="Nombre">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Cuenta Superior:</strong>
                                    <select type="text" name="id_cuenta_superior" class="form-control" placeholder="Cuenta Superior">
                                        <option value="null"tabindex="-1">Seleccione una opción...</option>
                                        @foreach($cuentas as $cuenta)
                                        <option value="{{ $cuenta->ID }}">{{ $cuenta->CODIGO }} - {{ $cuenta->NOMBRE }}</option>
                                        @endforeach
                                    
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Observaciones:</strong>
                                    <textarea name="observaciones" class="form-control" placeholder="Observaciones..."></textarea>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>PRESUPUESTO:</strong>
                                    <input type="number" name="presupuesto" class="form-control" placeholder="$ 0">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>PRESUPUESTO EJECUTIVO:</strong>
                                        <input type="number" name="presupuesto_ejecutivo" class="form-control" placeholder="$ 0">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>PRESUPUESTO LEGISLATIVO:</strong>
                                            <input type="number" name="presupuesto_legislativo" class="form-control" placeholder="$ 0">
                                        </div>
                                    </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Cuenta Principal:</strong>
                                    <input type="checkbox" name="is_principal" class="form-control" >
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Crear</button>
                                    <a class="btn btn-default" href="{{ route('cuentas.index') }}"> Regresar</a>
                            </div>
                        </div>


                    </form>
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>


@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
            $.ajax({
                url: '/buscarSgteNroCuenta',
                data: { "_token": "{{ csrf_token() }}",
                        id_cuenta_superior   : $('[name="id_cuenta_superior"]').val()
                },
                success: function(){
                    
                },
                error: function(){
                    
                }
            });
        });
    </script>
@stop