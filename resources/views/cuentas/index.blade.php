@extends('layouts.backend')

@section('css')
    <!-- Required Stylesheets -->
    {{-- <link href="/bower_components/bootstrap-treeview/src/css/bootstrap-treeview.css" rel="stylesheet"> --}}
    <link href="/backend/css/dataTables/datatables.min.css" rel="stylesheet">
    
@stop

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Cuentas</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                        <div class="col-lg-12">
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                        </div>
                    {{-- <div class="col-lg-12">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-12">
                        <a class="btn btn-sm btn-primary" href="{{ route('cuentas.create') }}" > Nueva Cuenta</a>
                    </div> --}}
                    {{-- <div class="col-lg-12 m-t-md">
                        <table class="table table-bordered">
                            <tr>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Observaciones</th>
                                <th>Saldo</th>
                                <th width="280px">Acción</th>
                            </tr>
                            @foreach ($cuentas as $cuenta)
                            <tr>
                                <td style="width: 20%;">{{ $cuenta->CODIGO }}</td>
                                <td style="width: 20%;">{{ $cuenta->NOMBRE }}</td>
                                <td style="width: 30%;">{{ $cuenta->OBSERVACIONES }}</td>
                                <td style="width: 10%;">{{ $cuenta->SALDO }}</td>
                                <td style="width: 20%;">
                                    <form action="{{ route('cuentas.destroy', $cuenta->ID ) }}" method="POST">
                                        <a title="Ver" class="btn btn-xs btn-info" href="{{ route('cuentas.show',$cuenta->ID) }}"><i class="fa fa-eye"></i></a>
                                        <a title="Editar" class="btn btn-xs btn-primary" href="{{ route('cuentas.edit',$cuenta->ID) }}"><i class="fa fa-edit"></i></a>
                                        @csrf
                                        @method('DELETE')
                                        <button title="Eliminar" type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        {!! $cuentas->links() !!}
                    </div> --}}
                    <div class="col-lg-12">
                          <div class="row">
                            {{-- <hr> --}}
                            <div class="col-sm-12">
                              
                              <div class="form-group row">
                                <div class="col-sm-12">
                                  <a class="btn btn-sm btn-primary" href="{{ route('cuentas.create') }}" > Nueva Cuenta</a>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-12">
                              

                                <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr>
                                
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Cuenta Superior</th>
                                <th>Presupuesto</th>
                                <th>Presupuesto Ejecutivo</th>
                                <th>Presupuesto Legislativo</th>
                                <th>Saldo</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            <?php
                              foreach($cuentas as $cuenta){
                                echo "<tr>";
                               
                                echo "<td>" . $cuenta->CODIGO ."</td>";
                                echo "<td>". $cuenta->NOMBRE ."</td>";
                                echo "<td>". $cuenta->nombreCuentaSuperior["CODIGO"] . ' - ' .$cuenta->nombreCuentaSuperior["NOMBRE"] ."</td>";
                                echo "<td>". number_format($cuenta->PRESUPUESTO,2,",",".") ."</td>";
                                echo "<td>". number_format($cuenta->PRESUPUESTO_EJECUTIVO,2,",",".") ."</td>";
                                echo "<td>". number_format($cuenta->PRESUPUESTO_LEGISLATIVO,2,",",".") ."</td>";
                                echo "<td>". number_format($cuenta->SALDO,2,",",".") ."</td>";
                            ?>
                                <td>
                                    <a href="/eliminarcuenta/<?php echo $cuenta->ID; ?>"><button title='Eliminar' class='btn btn-xs btn-danger eliminar'><i class='fa fa-trash'></i></button></a>
                                    <a href="/cuentas/<?php echo $cuenta->ID; ?>/edit"><button title='Editar' class='btn btn-xs btn-default'><i class='fa fa-edit'></i></button></a>
                                </td>
                            <?php
                                echo "</tr>";
                              }
                            ?>
                            
                            </tbody>
                            <tfoot>
                            <tr>
                                
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Cuenta Superior</th>
                                <th>Presupuesto</th>
                                <th>Presupuesto Ejecutivo</th>
                                <th>Presupuesto Legislativo</th>
                                <th>Saldo</th>
                                <th>Acción</th>
                            </tr>
                            </tfoot>
                            </table>
                                </div>
        
                            
                            </div>
                          </div>
                    </div>
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
    
    {{-- <script src="/bower_components/bootstrap-treeview/src/js/bootstrap-treeview.js"></script> --}}
    <script src="/backend/js/plugins/dataTables/datatables.min.js"></script>
    <script>
      $(document).ready(function(){
          $('.dataTables-example').DataTable({
              pageLength: 25,
              responsive: true,
              dom: '<"html5buttons"B>lTfgitp',
              buttons: [
                  { extend: 'copy'},
                  {extend: 'csv'},
                  {extend: 'excel', title: 'PlanDeCuentas'},
                  {extend: 'pdf', title: 'PlanDeCuentas'},

                  {extend: 'print',
                   customize: function (win){
                          $(win.document.body).addClass('white-bg');
                          $(win.document.body).css('font-size', '10px');

                          $(win.document.body).find('table')
                                  .addClass('compact')
                                  .css('font-size', 'inherit');
                  }
                  }
              ]

          });

          
          
          
          

      });

        
  </script>

@stop
