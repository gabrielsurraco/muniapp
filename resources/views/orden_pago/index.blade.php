@extends('layouts.backend')

@section('css')
    <!-- Required Stylesheets -->
    
    <link href="/backend/css/dataTables/datatables.min.css" rel="stylesheet">
    
@stop

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Órdenes de Pago</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                    </div>
                    <div class="col-lg-12">
                        @if (isset($id))
                            <div class="alert alert-success">
                            <p>Su Orden de Pago ha sido generado correctamente! <a href="/exportar-orden-pago/{{$id}}" target="_blank">- Descargar PDF -</a> <a href="/pagos/pagar-orden-pago/{{$id}}" target="_blank">- PAGAR ORDEN DE PAGO -</a></p>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-12">
                          <div class="row">
                            {{-- <hr> --}}
                            <div class="col-sm-12">
                              
                              <div class="form-group row">
                                <div class="col-sm-12">
                                  <a class="btn btn-sm btn-primary" href="{{ route('ordenes-de-pago.create') }}" > Nueva Órden</a>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-12">
                              

                                <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Monto</th>
                                <th>Proveedor</th>
                                <th>Saldo</th>
                                <th>Código</th>
                                <th>Concepto</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            <?php
                            
                              foreach($ordenes as $orden){
                                echo "<tr>";
                               
                                echo "<td>" . $orden->FECHA ."</td>";
                                echo "<td>". $orden->MONTO ."</td>";
                                echo "<td>". $orden->proveedor->NOMBRE ." - ". $orden->proveedor->CUIL."</td>";
                                echo "<td>". $orden->SALDO ."</td>";
                                echo "<td>". $orden->CODIGO ."</td>";
                                echo "<td>". substr($orden->CONCEPTO, 0, 10) ."</td>";
                            ?>
                                <td>
                                    <a href="/exportar-orden-pago/<?php echo $orden->ID; ?>"><button title='Descargar' class='btn btn-xs btn-success'><i class='fa fa-download'></i></button></a>
                                    <a href="/eliminarorden/<?php echo $orden->ID; ?>"><button title='Eliminar' class='btn btn-xs btn-danger eliminar'><i class='fa fa-trash'></i></button></a>
                                    @if($orden->SALDO > 0)
                                        <a href="/pagos/pagar-orden-pago/<?php echo $orden->ID; ?>"><button title='Pagar' class='btn btn-xs btn-info'><i class='fa fa-cart-plus'></i></button></a>
                                    @endif
                                    <!--<a href="/ordenes/<?php echo $orden->ID; ?>/edit"><button title='Editar' class='btn btn-xs btn-default'><i class='fa fa-edit'></i></button></a>-->
                                    
                                </td>
                            <?php
                                echo "</tr>";
                              }
                            ?>
                            
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Fecha</th>
                                <th>Monto</th>
                                <th>Proveedor</th>
                                <th>Saldo</th>
                                <th>Código</th>
                                <th>Concepto</th>
                                <th>Acción</th>
                            </tr>
                            </tfoot>
                            </table>
                                </div>
        
                            
                            </div>
                          </div>
                    </div>
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
    
    
    <script src="/backend/js/plugins/dataTables/datatables.min.js"></script>
    <script>
      $(document).ready(function(){
          $('.dataTables-example').DataTable({
              pageLength: 25,
              responsive: true,
              dom: '<"html5buttons"B>lTfgitp',
              buttons: [
                  { extend: 'copy'},
                  {extend: 'csv'},
                  {extend: 'excel', title: 'PlanDeCuentas'},
                  {extend: 'pdf', title: 'PlanDeCuentas'},

                  {extend: 'print',
                   customize: function (win){
                          $(win.document.body).addClass('white-bg');
                          $(win.document.body).css('font-size', '10px');

                          $(win.document.body).find('table')
                                  .addClass('compact')
                                  .css('font-size', 'inherit');
                  }
                  }
              ]

          });

          
          
          
          

      });

        
  </script>

@stop
