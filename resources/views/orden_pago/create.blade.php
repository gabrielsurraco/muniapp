@extends('layouts.backend')

@section('css')
    <link href="/backend/css/datapicker/datepicker3.css" rel="stylesheet">
    <link href="/backend/css/select2/select2.min.css" rel="stylesheet" />
    <link href="/backend/css/select2/select2-bootstrap.css" rel="stylesheet" />
@stop

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nueva Órden de Pago</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    
                    <div class="col-lg-12">
                            @if ($message = Session::get('danger'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                    </div>
                    
                    <form action="{{ route('ordenes-de-pago.store') }}" method="POST">
                        @csrf

                         <div class="row wrapper">
<!--                             <a href="skype:gabriel.surraco1?call">Call gABRIEL sURRACO</a>-->
                            <div class="col-xs-6 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <strong>Fecha:</strong>
                                    <input id="datepicker" autocomplete="off" type="text" name="fecha" value="{{ old('fecha') }}" class="form-control" placeholder="dd/mm/aaaa">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3">
                                <div class="form-group monto">
                                    <strong>Monto:</strong>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input  type="text" name="monto" value="{{ old('monto') }}" class="form-control dinero" placeholder="0,00">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Es una Orden de Pago BIS de:</strong>
                                    <select type="text" name="id_cuenta_superior" class="form-control" placeholder="Cuenta Superior">
                                        <option value="null"tabindex="-1">Seleccione una opción...</option>
                                        @foreach($ordenes as $orden)
                                        <option value="{{ $orden->ID }}">{{ $orden->CODIGO ." - ". $orden->proveedor->NOMBRE }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                         </div>
                        <div class="row wrapper">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Proveedor:</strong>
                                    
                                    <select  name="proveedor" class=" form-control js-example-basic-single" placeholder="Cuenta Superior" style="width: 100%;">
                                        <option value="null" tabindex="-1">Seleccione una opción...</option>
                                        @foreach($proveedores as $pro)
                                        <option value="<?php echo $pro->ID; ?>">{{ $pro->CODIGO ." - ". $pro->NOMBRE." - ". $pro->RAZON_SOCIAL }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Nro. de Comprobante recibido:</strong>
                                    <input name="comprobante" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Concepto:</strong>
                                    <input name="concepto" type="text" class="form-control" >
                                </div>
                            </div>
                            
                            

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <h3>Cuentas a Imputar</h3>
                                <a class="btn btn-md btn-success nuevo" >+ Nuevo</a>
                                <a class="btn btn-md btn-danger limpiar">Limpiar</a>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Cuenta</td>
                                            <td>Imputa</td>
                                            <td>Departamento</td>
                                            <td>Acción</td>
                                        </tr>
                                    </thead>
                                    <tbody id="bodytable">
                                        <tr class="tr-ejemplo hidden">
                                            <td><input type="hidden" name="cuentaid[]" /> <span>Cuenta 1</span></td>
                                            <td><input type="hidden" name="imputa[]" /><span>$ 200</span></td>
                                            <td><input type="hidden" name="departamento[]" /><span>Ejecutivo</span></td>
                                            <td>
                                                <a class="btn btn-xs btn-danger eliminar-fila"><i class="fa fa-eraser"></i></a>
                                                <a class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Observaciones:</strong>
                                    <textarea name="observaciones" class="form-control" placeholder="Observaciones..."></textarea>
                                </div>
                            </div>
                            
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Crear</button>
                                    <a class="btn btn-default" href="{{ route('cuentas.index') }}"> Regresar</a>
                            </div>
                        </div>


                    </form>
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>


<div class="modal inmodal fade" id="myModal6"  role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Seleccione Cuenta a Imputar</h4>
            </div>
            <div class="modal-body">
                <div class="form-group" >
                    <strong>Cuenta:</strong>
                    
                        <select name="cuenta_a_imputar" class="js-example-basic-single " style="width: 100%;">
                            
                            @foreach($cuentas as $cuenta)

                                @if($cuenta->IS_PRINCIPAL)
                                    <option style="font-weight: bold;" value="{{ $cuenta->ID }}">{{ $cuenta->CODIGO ." - ". $cuenta->NOMBRE }}</option>
                                @else
                                    <option  value="{{ $cuenta->ID }}">{{ $cuenta->CODIGO ." - ". $cuenta->NOMBRE }}</option>
                                @endif
                        
                            @endforeach

                        </select>
                    
                    {{-- <select type="text" name="cuenta_a_imputar" class="form-control" placeholder="Cuenta Superior">
                        
                        
                    </select> --}}
                </div>
                <div class="form-group">
                    <strong>Seleccione el departamento:</strong>
                    <select type="text" name="departamento" class="form-control" placeholder="Cuenta Superior">
                        <option value="ejecutivo">Ejecutivo</option>
                        <option value="legislativo">Legislativo</option>
                    </select>
                </div>
                <div class="form-group">
                    <strong>Monto a Imputar:</strong>
                    <input name="monto_a_imputar" type="text" value="0" placeholder="0.00" class="form-control dinero">
                </div>
<!--                <div class="form-group">
                    <strong>Monto a Cancelar:</strong>
                    <input name="monto_a_cancelar" type="text" value="0" placeholder="0.00" class="form-control dinero">
                </div>-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary boton-agregar">Agregar</button>
            </div>
        </div>
    </div>
</div>



@endsection

@section('javascript')

    <script src="/backend/js/plugins/select2/select2.full.min.js"></script>
    <script src="/backend/js/plugins/datapicker/locales/bootstrap-datepicker.es.js"></script>
    <script>
        $(function() {
            
            /* select2 */
            $.fn.select2.defaults.set( "theme", "bootstrap" );
            
            $('.js-example-basic-single').select2({
                placeholder: 'Select an option',                
            });
            //$(".js-example-basic-single").select2({ dropdownParent: "#myModal6" });
            
            var montoImputado = 0;
            
            $( "#datepicker" ).datepicker({
                format: 'dd/mm/yyyy',
            });
            
            $("#datepicker").datepicker("setDate", "0");
            
            $(".dinero").on("input", function() {
                // allow numbers, a comma or a dot
                var v= $(this).val(), vc = v.replace(/[^0-9\.]/, '');
                if (v !== vc)        
                    $(this).val(vc);
            });
            
            $('a.limpiar').click(function(){
                $('#bodytable tr:not(:first-child)').empty();
                montoImputado = 0;
            });
            
            $('a.nuevo').click(function(){
                
                var valor = $('input[name=monto]').val();
                if(valor === ''){
                    $('.monto').addClass('alert-danger');
                    $('a.nuevo').removeAttr('data-toggle');
                    $('a.nuevo').removeAttr('data-target');
                    alert('Favor ingrese un monto a la Orden de Pago');
                }else{
                    //Limpio los campos
                    $('select[name=cuenta_a_imputar] option:eq(0)').prop('selected', true);
                    $('select[name=departamento] option:eq(0)').prop('selected', true);
                    $('input[name=monto_a_imputar]').val(0);
                    
                    
                    
                    $('.monto').removeClass('alert-danger');
                    $('a.nuevo').attr('data-toggle','modal');
                    $('a.nuevo').attr('data-target','#myModal6');
                }
            });
            
            $('.boton-agregar').click(function(){
                
                montoImputado = montoImputado + parseInt($('input[name=monto_a_imputar]').val());
                
                if(montoImputado > parseInt($('input[name=monto]').val())){
                    alert("El monto imputado supera al TOTAL");
                    montoImputado = montoImputado - parseInt($('input[name=monto_a_imputar]').val());
                    return true;
                }
                
                var id_cuenta_imputar = $("select[name=cuenta_a_imputar] option:selected" ).val();
                var nombre_cuenta_imputar = $("select[name=cuenta_a_imputar] option:selected" ).text();
                
                var departamento = $("select[name=departamento] option:selected" ).val();
                
                var monto_a_imputar = $('input[name=monto_a_imputar]').val();
                
                $('#myModal6').modal('toggle');
                
                var body = $('#bodytable');
                var tr = $('.tr-ejemplo').clone(true);
                tr.removeClass('hidden');
                tr.removeClass('tr-ejemplo');
                tr.find('td:first span').text(nombre_cuenta_imputar);
                tr.find('td:first input').attr('value',id_cuenta_imputar);
                tr.find('td:eq(1) span').text("$ "+monto_a_imputar);
                tr.find('td:eq(1) input').attr('value',monto_a_imputar);
//                tr.find('td:eq(2) span').text("$ "+monto_a_cancelar);
//                tr.find('td:eq(2) input').attr('value',monto_a_cancelar);
                tr.find('td:eq(2) span').text(departamento);
                tr.find('td:eq(2) input').attr('value',departamento);
                body.append(tr);
                
            });
            
            $('.eliminar-fila').click(function(){
                var montoArestar = $(this).parent().parent().find("td:eq(1) input").attr('value');
                montoImputado = montoImputado - parseInt(montoArestar);
                $(this).parent().parent().remove();
            });


            
        });
  </script>
@stop