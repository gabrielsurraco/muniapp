<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Muni APP') }}</title>

    <link href="{{ asset('backend/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/datapicker/datepicker3.css') }}" rel="stylesheet">

    <link href="{{ asset('backend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/style.css') }}" rel="stylesheet">
    <style>
        .input-xs {
            height: 22px;
            padding: 2px 2px;
            font-size: 12px;
            line-height: 1.5; /* If Placeholder of the input is moved up, rem/modify this. */
            border-radius: 3px;
          }
    </style>



    @yield('css')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>

<body>

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                             </span> <span class="text-muted text-xs block">Bienvenido! <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Cerrar Sesión</a></li>
                            </ul>
                    </div>
                    <div class="logo-element">
                        10+
                    </div>
                </li>
                <li class="@if(Route::is('proveedores.index')) active @endif">
                    <a href="{{ url ('proveedores') }}"><i class="fa fa-user-circle"></i> <span class="nav-label">Proveedores </span></a>
                </li>
                <li class="@if(Route::is('ordenes-de-pago.index')) active @endif">
                    <a href="{{ url ('ordenes-de-pago') }}"><i class="fa fa-credit-card"></i> <span class="nav-label">Órdenes de Pago </span></a>
                </li>
                <li class="@if(Route::is('cuentas.index')) active @endif">
                    <a href="/cuentas"><i class="fa fa-address-card"></i> <span class="nav-label">Plan de Cuentas </span></a>
                </li>
                <li class="@if(Route::is('creditos-del-ejercicio.index')) active @endif">
                    <a href="{{ url ('creditos-del-ejercicio') }}"> <i class="fa fa-building-o" aria-hidden="true"></i> <span class="nav-label">Creditos del ejercicio </span></a>
                </li>
                <li class="@if(Route::is('pagos.index')) active @endif">
                    <a href="{{ url ('/pagos') }}"> <i class="fa fa-money" aria-hidden="true"></i> <span class="nav-label">Pagos </span></a>
                </li>

                

            </ul>

        </div>
    </nav>

    <div id="page-wrapper" style="background: #e8e8e8;">
        <div class="row border-bottom m-b-md hidden-print">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" method="post" action="#">
                        <div class="form-group">
                            <input type="text" placeholder="Buscar..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Cerrar Sesión
                        </a>
                    </li>
                </ul>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

            </nav>
        </div>


        @yield('content')

        <div class="footer hidden-print">
<!--            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>-->
            <div>
                <strong>Copyright</strong> MuniApp &copy; 2018
            </div>
        </div>

    </div>
</div>

<!-- Mainly scripts -->
<script src="{{ asset('/backend/js/plugins/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('/backend/js/plugins/bootstrap.min.js') }}"></script>
<script src="{{ asset('/backend/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('/backend/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('/backend/js/plugins/inspinia.js') }}"></script>
<script src="{{ asset('/backend/js/plugins/pace/pace.min.js') }}"></script>

<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('/backend/js/plugins/datapicker/locales/bootstrap-datepicker.es.js') }}"></script>
<!-- SweetAlert -->
<script src="{{ asset('/backend/js/plugins/sweetAlert/sweetalert.min.js') }}"></script>

<script src="{{ asset('/backend/js/plugins/funciones.js') }}"></script>

@yield('javascript')

</body>

</html>
