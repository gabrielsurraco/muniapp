<?php setlocale(LC_ALL,"es_ES"); ?>
<style>
    #sub-left {
        float: left;
        /*border: 1px solid black;*/
        width: 70%;
        height: 14%;
    }
    #sub-right {
       float: right;
       /*border: 1px solid black;*/
       font-family: arial, sans-serif;
       width: 70%;
       height: 14%;
    }
    .clear-both {
       clear: both;
    }
    
    table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}
div.imgg {
  width: 200px;
  height: 200px;
  display: block;
  position: relative;
}

div.imgg::after {
  content: "";
  background-image: url('img/logo_cerro_azul.jpg');
  opacity: 0.5;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  position: absolute;
  z-index: -1;   
}

#watermark {
    position: fixed;
    top: 45%;
    width: 100%;
    text-align: center;
    opacity: .6;
    transform: rotate(10deg);
    transform-origin: 50% 50%;
    z-index: -1000;
  }

/*tr:nth-child(even) {
    background-color: #dddddd;
}*/
</style>
<div id="sub-title" style="position: fixed; ">
    <div style="height: auto; width: 100%;">
        <label style="width: 50%; float: left;">MUNICIPALIDAD DE CERRO AZÚL, MISIONES</label>
        <label style="float: right; width: 50%; text-align: right; font-weight: bold;"><?php  echo ucfirst(strftime("%A %d de %B del %Y")); ?></label>
    </div>
    
    
    {{-- <div id="sub-left">
        <p>MUNICIPALIDAD DE CERRO AZÚL, MISIONES</p>
    </div>
    
    <div id="sub-right">
        <u><i><h2 style="margin-top: 45px; margin-left: 35px;">ORDEN DE PAGO</h2></i></u>
    </div> --}}
    
</div>
<div class="clear-both" >
    <p style="text-align: center;"> <strong style="font-size: 22px;">ORDEN DE PAGO NRO.: </strong> <strong style="font-size: 22px;">{{ $orden_pago->CODIGO }} <?php echo ($orden_pago->ID_SUPERIOR == 0) ? '' : '/ '.$orden_pago->CODIGO_BIS . ' (BIS)';  ?></strong></p>
    
    <p style="font-size: 14px;">DE CONFORMIDAD CON LAS DOCUMENTACIONES RESPECTIVAS, SE LIQUIDA PARA ABONAR POR TESORERÍA MUNICIPAL A FAVOR DE:</p>
<strong style="padding-left: 25px; font-size: 14px;">{{ strtoupper($orden_pago->proveedor->RAZON_SOCIAL) }} - CUIL/T: {{ $orden_pago->proveedor->CUIL }}</strong><br/>
<p style="font-size: 14px;"><strong>LA CANTIDAD DE PESOS:</strong> $   {{ $orden_pago->MONTO }} ({{ $monto_letras }})</p>
<p style="font-size: 14px;"><strong >EN CONCEPTO DE:</strong>   {{ $orden_pago->CONCEPTO }}</p>

<table>
    <thead>
        <tr style="background-color: #dddddd; font-weight: bold;">
            <td style="text-align: center;">Tipo Imputación</td>
            <td style="text-align: center;">Cuenta Principal</td>
            <td style="text-align: center;">Cuenta Secundaria</td>
            <td style="text-align: center;">Monto</td>
        </tr>
    </thead>
    <tbody>
        
        @foreach($orden_pago->detalles as $detalle)
            <tr>
                <td>Imputación definitiva</td>
                <td><strong style="font-size: 14px;">{{ $detalle->cuenta->cuentasuperior->NOMBRE }}</strong></td>
                <td style="font-size: 14px;">{{ $detalle->cuenta->NOMBRE }}</td>
                <td style="text-align: center;">{{ $detalle->MONTO }}</td>
            </tr>
        @endforeach
        <tr style="background-color: #dddddd; font-weight: bold;">
            <td colspan="3" style="text-align: center; "><strong>TOTAL</strong></td>
            <td style="text-align: center; "><strong>$ {{ $orden_pago->MONTO }}</strong></td>
        </tr>
        
        
    </tbody>
</table>
    
</div>
<div style="margin-top:30%;  position: fixed; padding-top: 720px; height: auto; width: 100%;">
        <hr>Vista la liquidación que antecede, téngase por orden de pago y abónese de conformidad a la misma, gírese a la Tesorería Municipal para su cumplimiento.
        <p style="padding-left: 40%;margin-top: 3px;">CERRO AZUL, MISIONES <?php  echo ucfirst(strftime("%A %d de %B del %Y")); ?></p>
        <hr>
        <strong>Comprobantes / Referencias:</strong>
        <p>{{ $orden_pago->NRO_COMP_RECI }} / {{ $orden_pago->OBSERVACIONES }}</p>
        <p style="padding-left: 70%;margin-top: 3px;">------------------------------</p>
</div>
<script type="text/php">
    if ( isset($pdf) ) {
        $font = Font_Metrics::get_font("helvetica", "bold");
        $pdf->page_text(500, 18, "Página: {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0,0,0));
    }
</script>