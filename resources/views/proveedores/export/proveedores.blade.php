<style>
    #sub-left {
        float: left;
        /*border: 1px solid black;*/
        width: 27%;
        height: 14%;
    }
    #sub-right {
       float: right;
       /*border: 1px solid black;*/
       width: 70%;
       height: 14%;
    }
    .clear-both {
       clear: both;
    }
    
    table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<div class="clear-both">
    <table>

        <tr>
          <th >CÓDIGO</th>
          <th><strong>Apellido y Nombre</strong></th>
            <th ><strong>RAZÓN SOCIAL</strong></th>
            <th ><strong>CUIL</strong></th>
            <th ><strong>DOMICILIO</strong></th>
            <th ><strong>TELÉFONO</strong></th>
            <th ><strong>EMAIL</strong></th>
            <th ><strong>OBSERVACIONES</strong></th>
        </tr>
        @foreach($proveedores as $proveedor)
            <tr>
                <td>{{ $proveedor->CODIGO }}</td>
                <td>{{ $proveedor->NOMBRE }}</td>
                <td>{{ $proveedor->RAZON_SOCIAL }}</td>
                <td>{{ $proveedor->CUIL }}</td>
                <td>{{ $proveedor->DOMICILIO }}</td>
                <td>{{ $proveedor->TELEFONO }}</td>
                <td>{{ $proveedor->EMAIL }}</td>
                <td>{{ $proveedor->OBSERVACIONES }}</td>
            </tr>
        @endforeach
      </table>
</div>

    

<script type="text/php">
    if ( isset($pdf) ) {
//        $font = Font_Metrics::get_font("helvetica", "bold");
        $pdf->page_text(500, 18, "Página: {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0,0,0));
    }
</script>