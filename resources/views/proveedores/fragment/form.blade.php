<div class="row">
    <div class="col-md-6">
        <div class="form-group">
                <label for="CODIGO">Codigo del proveedor:</label>
                <input type="number" value="<?php echo (isset($proveedor->CODIGO)) ? $proveedor->CODIGO : ''; ?>" name="CODIGO" id="CODIGO" class="form-control" />
        </div>

        <div class="form-group">
                <label for="nombre">Apellido y Nombre:</label>
                <input type="text" value="<?php echo (isset($proveedor->NOMBRE)) ? $proveedor->NOMBRE : ''; ?>" name="NOMBRE" id="nombre" class="form-control" />
        </div>

        <div class="form-group">
                <label for="razon_social">Razon Social:</label>
                <input type="text" value="<?php echo (isset($proveedor->RAZON_SOCIAL)) ? $proveedor->RAZON_SOCIAL : ''; ?>" name="RAZON_SOCIAL" id="razon_social" class="form-control" />
        </div>

        <div class="form-group">
                <label for="cuil">CUIL:</label>
                <input type="text" value="<?php echo (isset($proveedor->CUIL)) ? $proveedor->CUIL : ''; ?>" name="CUIL" id="cuil" class="form-control" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
                <label for="domicilio">Domicilio:</label>
                <input type="text" value="<?php echo (isset($proveedor->DOMICILIO)) ? $proveedor->DOMICILIO : ''; ?>" name="DOMICILIO" id="domicilio" class="form-control" />
        </div>

        <div class="form-group">
                <label for="telefono">Telefono:</label>
                <input type="number" value="<?php echo (isset($proveedor->TELEFONO)) ? $proveedor->TELEFONO : ''; ?>" name="TELEFONO" id="telefono" class="form-control" />
        </div>

        <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" value="<?php echo (isset($proveedor->EMAIL)) ? $proveedor->EMAIL : ''; ?>" name="EMAIL" id="email" class="form-control" />
        </div>

        <div class="form-group">
                <label for="observaciones">Observacion:</label>
                <textarea name="OBSERVACIONES"  id="observaciones" class="form-control"><?php echo (isset($proveedor->OBSERVACIONES)) ? $proveedor->OBSERVACIONES : ''; ?></textarea>
        </div>

        
    </div>
    <div class="col-md-12 m-t-md">
        <div class="form-group col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Guardar</button>
             <a class="btn btn-default" href="{{ route('proveedores.index') }}"> Regresar</a>
        </div>
    </div>
</div>



