@extends('layouts.backend')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Proveedor</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    
                    
                    <form action="#" method="POST">
                        @csrf
                         <div class="row wrapper">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Código:</strong>
                                    <input value="{{ $proveedor->CODIGO }}" type="number" name="codigo" class="form-control" placeholder="Código" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Nombre:</strong>
                                    <input value="{{ $proveedor->NOMBRE }}" type="text" name="nombre" class="form-control" placeholder="Nombre" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                               <div class="form-group">
                                    <strong>Razón Social:</strong>
                                    <input value="{{ $proveedor->RAZON_SOCIAL }}" type="text" name="razon_social" class="form-control" placeholder="Razon_social" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>CUIL:</strong>
                                    <input value="{{ $proveedor->CUIL }}" type="number" name="cuil" class="form-control" placeholder="CUIL" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                               <div class="form-group">
                                    <strong>Domicilio:</strong>
                                    <input value="{{ $proveedor->DOMICILIO }}" type="text" name="domicilio" class="form-control" placeholder="Domicilio" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Teléfono:</strong>
                                    <input value="{{ $proveedor->TELEFONO }}" type="number" name="telefono" class="form-control" placeholder="Telefono" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Email:</strong>
                                    <input value="{{ $proveedor->EMAIL }}" type="email" name="email" class="form-control" placeholder="Email" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Observaciones:</strong>
                                    <textarea name="observaciones" class="form-control" placeholder="Observaciones..." readonly="">{{ $proveedor->OBSERVACIONES }}</textarea>
                                </div>
                            </div>
                            
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <a class="btn btn-primary" href="{{ route('proveedores.index') }}"> Regresar</a>
                            </div>
                        </div>


                    </form>
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>








    
    
    

@endsection