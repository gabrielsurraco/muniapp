@extends('layouts.backend')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo proveedor</h5>
            </div>
            <div class="ibox-content">
                <div class="row wrapper">
                    
                    
                    
                    <form action="{{ route('proveedores.store' ) }}" method="POST">
                        {{ csrf_field() }}
                        @include('proveedores.fragment.form')
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>


@endsection