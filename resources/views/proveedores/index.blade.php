@extends('layouts.backend')

@section('css')
	<link href="{{ asset('/backend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
@stop
	
@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Proveedores</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-12">
                        <a class="btn btn-sm btn-success" href="{{ route('proveedores.create') }}" ><i class="fa fa-plus"></i> Nuevo</a>
                        <a class="btn btn-sm btn-primary" href="{{ url ('exportarExcel') }}"><i class="fa fa-file-excel-o"></i> Excel</a>
                        <a class="btn btn-sm btn-danger" href="{{ url ('exportarPDF') }}"><i class="fa fa-file-pdf-o"></i> Pdf</a>
                        
                    </div>
                    <div class="col-lg-10">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <tr>
                                <th>Código</th>
                                <th>Apellido y Nombre</th>
                                <th>Razon Social</th>
                                <th>Cuil</th>
                                <th>Acción</th>
                            </tr>
                            @foreach ($proveedores as $proveedor)
                            <tr>
                                <td style="width: 10%;">{{ $proveedor->CODIGO }}</td>
                                <td style="width: 20%;">{{ $proveedor->NOMBRE }}</td>
                                <td style="width: 20%;">{{ $proveedor->RAZON_SOCIAL }}</td>
                                <td style="width: 20%;">{{ $proveedor->CUIL }}</td>
                                <td style="width: 10%;">
                                    <form action="{{ route('proveedores.destroy', $proveedor->ID ) }}" method="POST">
                                        <a title="Ver" class="btn btn-xs btn-info" href="{{ route('proveedores.show',$proveedor->ID) }}"><i class="fa fa-eye"></i></a>
                                        <a title="Editar" class="btn btn-xs btn-primary" href="{{ route('proveedores.edit',$proveedor->ID) }}"><i class="fa fa-edit"></i></a>
                                        @csrf
                                        @method('DELETE')
                                        <button title="Eliminar" type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                                
                            </tr>
                            @endforeach
                        </table>
                        {!! $proveedores->links() !!}
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>




@endsection

@section('javascrip')
	<script src="{{ asset('/backend/js/jquery-3.1.1.min.js') }}"></script> 
    <script src="{{ asset('/backend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/backend/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('/backend/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('/backend/js/inspinia.js') }}"></script>
    <script src="{{ asset('/backend/js/plugins/pace/pace.min.js') }}"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
@stop