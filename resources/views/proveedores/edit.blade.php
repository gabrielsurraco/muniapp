@extends('layouts.backend')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Editar proveedor</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Ups!</strong> Hay algunos inconvenientes en el formulario.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    <div class="row wrapper">
                        <form action="{{ route('proveedores.update', $proveedor->ID) }}" method="POST">
                            @csrf
                            @method('PUT')
                            @include('proveedores.fragment.form')
                        </form>
                    </div>
                    
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>


@endsection