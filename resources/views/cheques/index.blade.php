@extends('layouts.backend')

@section('css')
	<link href="{{ asset('/backend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
@stop
	
@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Cheques</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-12">
                        <a class="btn btn-sm btn-primary" href="{{ route('cheques.create') }}" > Nueva Chequera</a>
                        <a class="btn btn-sm btn-primary" href="{{ url ('exportarExcel') }}">Exportar EXCEL</a>
                        <a class="btn btn-sm btn-primary" href="{{ url ('exportarPDF') }}">Exportar PDF</a>
                        <!--<a class="btn btn-sm btn-primary" href="{{ route('cuentas.create') }}" data-toggle="modal" data-target="#myModal"> Nueva Cuenta</a>-->
                        
                    </div>
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <tr>
                                <th>Código</th>
                                <th>N° Cuenta</th>
                                <th>Sucursal</th>
                                <th>Nombre</th>
                                <th>N° Inicial</th>
                                <th>N° Final</th>
                                
                            </tr>
                            @foreach ($cheques as $cheque)
                            <tr>
                                <td style="width: 30%;">{{ $cheque->CODIGO }}</td>
                                <td style="width: 30%;">{{ $cheque->NUMERO_CUENTA }}</td>
                                <td style="width: 20%;">{{ $cheque->SUCURSAL }}</td>
                                <td style="width: 20%;">{{ $cheque->NOMBRE }}</td>
                                <td style="width: 20%;">{{ $cheque->NUMERO_INICIAL }}</td>
                                <td style="width: 20%;">{{ $cheque->NUMERO_FINAL }}</td>
                                <td style="width: 10%;">
                                    <form action="{{ route('cheques.destroy', $cheque->ID ) }}" method="POST">
                                        <a title="Ver" class="btn btn-xs btn-info" href="{{ route('cheques.show',$cheque->ID) }}"><i class="fa fa-eye"></i></a>
                                        <a title="Editar" class="btn btn-xs btn-primary" href="{{ route('cheques.edit',$cheque->ID) }}"><i class="fa fa-edit"></i></a>
                                        @csrf
                                        @method('DELETE')
                                        <button title="Eliminar" type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                                
                            </tr>
                            @endforeach
                        </table>
                        {!! $cheques->links() !!}
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>




@endsection

@section('javascrip')
	<script src="{{ asset('/backend/js/jquery-3.1.1.min.js') }}"></script> 
    <script src="{{ asset('/backend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/backend/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('/backend/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('/backend/js/inspinia.js') }}"></script>
    <script src="{{ asset('/backend/js/plugins/pace/pace.min.js') }}"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
@stop