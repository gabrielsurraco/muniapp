@extends('layouts.backend')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo cheque</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    
                    
                    @include('cheques.fragment.error')

                    {!! Form::open(['route' => 'cheques.store']) !!}

                        @include('cheques.fragment.form')

                    {!! Form::close() !!}
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>


@endsection