<div class="form-group">
	{!! Form::label('CODIGO', 'Código') !!}
	{!! Form::number('CODIGO', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('NUMERO_CUENTA', 'N° Cuenta') !!}
	{!! Form::number('NUMERO_CUENTA', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('SUCURSAL', 'Sucursal del cheque') !!}
	{!! Form::text('SUCURSAL', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('nombre', 'Nombre') !!}
	{!! Form::text('NOMBRE', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('NUMERO_INICAL', 'N° Inicial') !!}
	{!! Form::number('NUMERO_INICAL', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('NUMERO_FINAL', 'N° Final') !!}
	{!! Form::number('NUMERO_FINAL', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-xs-12 col-sm-12 col-md-12 text-center">
	{!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
</div>



