@extends('layouts.backend')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Editar chequera</h5>
                <a href="{{ route('proveedores.index') }}" class="btn btn-default pull-right">Lista</a>
            </div>
            <div class="ibox-content">
                <div class="row">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hay algunos inconvenientes en el formulario.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    {!! Form::model($cheque, ['route' => ['cheques.update', $cheque->ID], 'method' => 'PUT']) !!}

                        @include('cheques.fragment.form')

                    {!! Form::close() !!}
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>


@endsection