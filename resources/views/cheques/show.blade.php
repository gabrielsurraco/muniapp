@extends('layouts.backend')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Chequera</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    
                    
                    <form action="#" method="POST">
                        @csrf

                         <div class="row wrapper">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Código:</strong>
                                    <input value="{{ $cheque->NUMERO_CUENTA }}" type="number" name="numero_cuenta" class="form-control" placeholder="Numero_cuenta" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>N° Cuenta:</strong>
                                    <input value="{{ $cheque->NUMERO_CUENTA }}" type="number" name="numero_cuenta" class="form-control" placeholder="Numero_cuenta" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Sucursal:</strong>
                                    <input value="{{ $cheque->SUCURSAL }}" type="text" name="sucursal" class="form-control" placeholder="Sucursal" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                               <div class="form-group">
                                    <strong>Nombre:</strong>
                                    <input value="{{ $cheque->NOMBRE }}" type="text" name="nombre" class="form-control" placeholder="Nombre" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Numero Inicial:</strong>
                                    <input value="{{ $cheque->NUMERO_INICIAL }}" type="number" name="numero_inicial" class="form-control" placeholder="Numero_inicial" readonly="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Numero Final:</strong>
                                    <input value="{{ $cheque->NUMERO_FINAL }}" type="number" name="numero_final" class="form-control" placeholder="Numero_final" readonly="">
                                </div>
                            </div>
                            
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <a class="btn btn-default" href="{{ route('cheques.index') }}"> Regresar</a>
                            </div>
                        </div>


                    </form>
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>








    
    
    

@endsection