@extends('layouts.backend')

@section('css')
    <link href="/backend/css/datapicker/datepicker3.css" rel="stylesheet">
@stop

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nueva Órden de Pago</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hay algunos inconvenientes en el formulario.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    <form action="{{ route('creditos-del-ejercicio.store') }}" method="POST">
                        @csrf

                         <div class="row wrapper">
                            <div class="col-xs-6 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <strong>Fecha:</strong>
                                    <input type="date" name="fecha" >
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <strong>Cuenta:</strong>
                                    <select type="text" name="fk_cuenta" class="form-control" placeholder="Cuenta Superior">
                                        <option value="null"tabindex="-1">Seleccione una cuenta...</option>
                                        @foreach($cuentas as $cuenta)
                                            <option value="{{ $cuenta->ID }}">{{ $cuenta->CODIGO }} - {{ $cuenta->NOMBRE }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <strong>Departamento:</strong>
                                    <div class="input-group">
                                        <input  type="radio" name="departamento" value = "1" />Ejecutivo

                                        <input  type="radio" name="departamento" value = "2" />Legislativo
                                    </div>
                                </div>
                            </div> 

                            <div class="col-xs-6 col-sm-2 col-md-2">
                                <div class="form-group monto">
                                    <strong>Monto:</strong>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input  type="text" name="monto" class="form-control dinero" placeholder="0,00">
                                    </div>
                                </div>
                            </div> 
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Observaciones:</strong>
                                    <textarea name="observaciones" class="form-control" placeholder="Observaciones..."></textarea>
                                </div>
                            </div>
                        
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Crear</button>
                                    <a class="btn btn-default" href="{{ route('creditos-del-ejercicio.index') }}"> Regresar</a>
                            </div>

                        </div>


                    </form>
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>


@endsection

@section('javascript')
    <script src="/backend/js/plugins/datapicker/locales/bootstrap-datepicker.es.js"></script>
    <script>
        $(function() {
            
            var montoImputado = 0;
            
            $( "#datepicker" ).datepicker();
            
            $(".dinero").on("input", function() {
                // allow numbers, a comma or a dot
                var v= $(this).val(), vc = v.replace(/[^0-9\.]/, '');
                if (v !== vc)        
                    $(this).val(vc);
            });
            
            $('a.limpiar').click(function(){
                $('#bodytable tr:not(:first-child)').empty();
                montoImputado = 0;
            });
            
            $('a.nuevo').click(function(){
                
                var valor = $('input[name=monto]').val();
                if(valor === ''){
                    $('.monto').addClass('alert-danger');
                    $('a.nuevo').removeAttr('data-toggle');
                    $('a.nuevo').removeAttr('data-target');
                    alert('Favor ingrese un monto a la Orden de Pago');
                }else{
                    //Limpio los campos
                    $('select[name=cuenta_a_imputar] option:eq(0)').prop('selected', true);
                    $('select[name=departamento] option:eq(0)').prop('selected', true);
                    $('input[name=monto_a_imputar]').val(0);
                    
                    
                    
                    $('.monto').removeClass('alert-danger');
                    $('a.nuevo').attr('data-toggle','modal');
                    $('a.nuevo').attr('data-target','#myModal6');
                }
            });
            
            $('.boton-agregar').click(function(){
                
                montoImputado = montoImputado + parseInt($('input[name=monto_a_imputar]').val());
                
                if(montoImputado > parseInt($('input[name=monto]').val())){
                    alert("El monto imputado supera al TOTAL");
                    return true;
                }
                
                var id_cuenta_imputar = $("select[name=cuenta_a_imputar] option:selected" ).val();
                var nombre_cuenta_imputar = $("select[name=cuenta_a_imputar] option:selected" ).text();
                
                var departamento = $("select[name=departamento] option:selected" ).val();
                
                var monto_a_imputar = $('input[name=monto_a_imputar]').val();
                
                $('#myModal6').modal('toggle');
                
                var body = $('#bodytable');
                var tr = $('.tr-ejemplo').clone(true);
                tr.removeClass('hidden');
                tr.removeClass('tr-ejemplo');
                tr.find('td:first').text(nombre_cuenta_imputar);
                tr.find('td:first').attr('id_cuenta',id_cuenta_imputar);
                tr.find('td:eq(1)').text("$ "+monto_a_imputar);
                tr.find('td:eq(1)').attr('monto',monto_a_imputar);
                tr.find('td:eq(2)').text(departamento);
                tr.find('td:eq(2)').attr('departamento',departamento);
                body.append(tr);
                
            });
            
            $('.eliminar-fila').click(function(){
                $(this).parent().parent().remove();
            });
            
        });
  </script>
@stop