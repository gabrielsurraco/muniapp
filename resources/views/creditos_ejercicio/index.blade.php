@extends('layouts.backend')

@section('css')
  <link href="{{ asset('/backend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
@stop
  
@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Creditos del ejercicio</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-12">
                        <a class="btn btn-sm btn-success" href="{{ route('creditos-del-ejercicio.presupuesto') }}" ><i class="fa fa-plus"></i> Presupuesto Inicial </a>

                        <a class="btn btn-sm btn-primary" href="{{ route('creditos-del-ejercicio.create') }}" ><i class="fa fa-plus"></i> Aumento Presupuestario </a>

                        <a class="btn btn-sm btn-danger" href="{{ route('creditos-del-ejercicio.create') }}" ><i class="fa fa-plus"></i> Compensación </a>

                        <a class="btn btn-sm btn-warning" href="{{ route('creditos-del-ejercicio.create') }}" ><i class="fa fa-sticky-note-o"></i> Reportes </a>
                    </div>
                    <div class="col-lg-10">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <tr>
                                <th>Fecha</th>
                                <th>Cuenta</th>
                                <th>Monto</th>
                                <th>Observación</th>
                            </tr>
                            <tbody>
                            
                            <?php
                                foreach ($movcuentas as $movcuenta) {
                                    echo "<tr>";
                                    if ($movcuenta->TIPO_MOV != "PRESUPUESTO_INICIAL") {
                                        echo "<td>" . $movcuenta->FECHA . "</td>";
                                        echo "<td>" . $movcuenta->rcuenta["NOMBRE"] . "</td>";
                                        echo "<td>" . $movcuenta->MONTO . "</td>";
                                        echo "<td>" . $movcuenta->OBSERVACIONES . "</td>";
                                    }
                                    
                            ?>

                            <?php
                                echo "</tr>";
                                }
                            ?>
                            
                            </tbody>
                        </table>
                        
                       
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>




@endsection

@section('javascrip')
  <script src="{{ asset('/backend/js/jquery-3.1.1.min.js') }}"></script> 
    <script src="{{ asset('/backend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/backend/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('/backend/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('/backend/js/inspinia.js') }}"></script>
    <script src="{{ asset('/backend/js/plugins/pace/pace.min.js') }}"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
@stop