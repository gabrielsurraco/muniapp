<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/exportarExcel', 'HomeController@export');
Route::get('/exportarPDF', 'HomeController@exportarPDF');

Route::resource('/cuentas','PlanCuentasController');

Route::resource('/proveedores','ProovedoresController');
Route::resource('/ordenes-de-pago','OrdenPagoController');
Route::get('/exportar-orden-pago/{id}', 'OrdenPagoController@exportarOrdenPago');

Route::get('/ver-op',function(){
    return view('reportes.orden_pago');
   // $letras = NumeroALetras::convertir(12345);
    //echo $letras;
});

Route::get('/exportar-orden-pago/{id}','OrdenPagoController@exportarOrdenPago');

Route::get('/exportarExcel', 'ProovedoresController@export');
Route::get('/exportarPDF', 'ProovedoresController@exportarPDF');

Route::get('/eliminarcuenta/{id}', 'PlanCuentasController@eliminarCuenta');
Route::post('/actualizarcuenta/', 'PlanCuentasController@actualizarCuenta')->name('actualizarcuenta');

//Route::get('/orden-de-pago','OrdenPagoController@index');

//Creditos de ejercicios 

Route::resource('/creditos-del-ejercicio','CreditoEjercicioController');
Route::get('/creditos-del-ejercicio.presupuesto','CreditoEjercicioController@presupuesto')->name('creditos-del-ejercicio.presupuesto');
Route::get('/creditos-del-ejercicio.compensacion','CreditoEjercicioController@compensacion')->name('creditos-del-ejercicio.compensacion');

Route::get('/pagos/pagar-orden-pago/{id}' , 'PagosController@payOrder')->name('pagos.create');

Route::post('/pagos/guardar-pago' , 'PagosController@savePayment')->name('pagos.guardar');

Route::get('/pagos','PagosController@index')->name('pagos.index');
Route::post('/pagos/disponibilidad-cheque' , 'PagosController@verificarDisponibilidadCheque');

