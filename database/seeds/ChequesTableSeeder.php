<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChequesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('CHEQUES')->insert([
            'NUMERO_CHEQUE'     => 1,
            'FK_CHEQUERA'       => 1,
            'FECHA_EMISION'     => '2018-10-16',
            'FECHA_CONTABLE'    => null,
            'IMPORTE'           => 66
        ]);
        DB::table('CHEQUES')->insert([
            'NUMERO_CHEQUE'     => 2,
            'FK_CHEQUERA'       => 1,
            'FECHA_EMISION'     => '2018-10-16',
            'FECHA_CONTABLE'    => null,
            'IMPORTE'           => 80
        ]);
        DB::table('CHEQUES')->insert([
            'NUMERO_CHEQUE'     => 3,
            'FK_CHEQUERA'       => 1,
            'FECHA_EMISION'     => '2018-10-16',
            'FECHA_CONTABLE'    => null,
            'IMPORTE'           => 90
        ]);


    }
}
