<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'          => 'Gabriel Surraco',
            'email'         => 'gabrielsurraco1@gmail.com',
            'password'      => bcrypt('admin123'),
        ]);
    }
}
