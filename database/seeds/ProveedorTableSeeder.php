<?php

use Illuminate\Database\Seeder;

class ProveedorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('PROVEEDORES')->insert([
            'FECHA_CREACION'   => '2018-10-01',
            'CODIGO'             => '1',
            'NOMBRE'             => 'Juanito SA',
            'RAZON_SOCIAL'             => 'Juanito SA',
            'CUIL'             => '20344774863',
        ]);
    }
}
