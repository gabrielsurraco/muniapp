<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(BancosTableSeeder::class);
        $this->call(ChequerasTableSeeder::class);
        $this->call(ProveedorTableSeeder::class);
        $this->call(ChequesTableSeeder::class);
    }
}
