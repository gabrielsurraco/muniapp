<?php

use Illuminate\Database\Seeder;
use App\Models\Banco;
use Illuminate\Support\Facades\DB;

class BancosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('BANCOS')->insert([
            'CODIGO'        => 1,
            'NOMBRE'        => 'BANCO MACRO',
            'DIRECCION'     => 'Posadas Misiones',
            'NUMERO_CUENTA' => '0001-00032',
        ]);
    }
}
