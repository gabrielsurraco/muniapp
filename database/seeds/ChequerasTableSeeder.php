<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChequerasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('CHEQUERAS')->insert([
            'NUMERO_CHEQUERA'   => 1,
            'DESDE'             => 1,
            'HASTA'             => 100,
            'FK_BANCO'          => 1,
            'NOMBRE'            => 'CHEQUERA BANCO MACRO'
        ]);
    }
}
