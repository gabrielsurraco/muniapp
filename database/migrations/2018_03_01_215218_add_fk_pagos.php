<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pagos', function (Blueprint $table) {
            $table->foreign('FK_ORDEN_PAGO')->references('ID')->on('ORDENES_DE_PAGO');
            //$table->foreign('FK_BANCO')->references('ID')->on('BANCOS');
//            $table->foreign('FK_CHEQUE')->references('ID')->on('CHEQUES');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pagos', function (Blueprint $table) {
            $table->dropForeign('pagos_fk_orden_pago_foreign');
            //$table->dropForeign('pagos_fk_banco_foreign');
//            $table->dropForeign('pagos_fk_cheque_foreign');
        });
    }
}
