<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MOV_DE_CUENTAS', function (Blueprint $table) {
            $table->increments('ID');
            $table->boolean('IS_INGRESO');
            $table->date('FECHA')->nullable()->default(null);
            $table->decimal('MONTO',18,2)->default(0);
            $table->integer('CUENTA_ORIGEN_ID')->nullable();
            $table->integer('FK_CUENTA')->unsigned();
            $table->integer('FK_CUENTA_DEBITAR')->unsigned()->default(0);
            $table->string('OBSERVACIONES')->nullable();
            $table->enum('DEPARTAMENTO',array('EJECUTIVO','LEGISLATIVO'))->nullable();
            $table->enum('TIPO_MOV',array('PAGO','MODIF_PRESUPUESTO','AUMENTO_PRESUPUESTO','PRESUPUESTO_INICIAL','COMPENSACION'))->nullable();
            $table->string('CREATED_BY')->nullable()->default(null);
            $table->string('UPDATED_BY')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MOV_DE_CUENTAS');
    }
}
