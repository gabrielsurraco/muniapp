<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkProveedorToCtaCte extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cuenta_corriente', function (Blueprint $table) {
            $table->foreign('FK_PROVEEDOR')->references('ID')->on('proveedores');
            $table->foreign('FK_ORDEN_PAGO')->references('ID')->on('ORDENES_DE_PAGO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cuenta_corriente', function (Blueprint $table) {
            $table->dropForeign('cuenta_corriente_fk_proveedor_foreign');
            $table->dropForeign('cuenta_corriente_fk_orden_pago_foreign');
        });
    }
}
