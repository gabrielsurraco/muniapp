<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PAGOS', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('FK_ORDEN_PAGO')->unsigned();
            $table->date('FECHA');
            $table->decimal('MONTO',18,2);
            $table->enum('TIPO_PAGO',array('DEBITO','CHEQUE','TRANSFERENCIA'))->nullable();
            $table->integer('FK_BANCO')->unsigned()->default(0);
            $table->integer('FK_CHEQUE')->unsigned()->default(0);
            $table->string('CREATED_BY')->nullable();
            $table->string('UPDATED_BY')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PAGOS');
    }
}
