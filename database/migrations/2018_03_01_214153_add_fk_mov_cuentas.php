<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkMovCuentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mov_de_cuentas', function (Blueprint $table) {
            $table->foreign('FK_CUENTA')->references('ID')->on('CUENTAS');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mov_de_cuentas', function (Blueprint $table) {
            $table->dropForeign('mov_de_cuentas_fk_cuenta_foreign');
        });
    }
}
