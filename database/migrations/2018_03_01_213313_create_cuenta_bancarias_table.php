<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentaBancariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CUENTAS_BANCARIAS', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('FK_BANCO')->unsigned();
            $table->string('NRO_CUENTA');
            $table->string('ALIAS');
            $table->string('CREATED_BY');
            $table->string('UPDATED_BY');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CUENTAS_BANCARIAS');
    }
}
