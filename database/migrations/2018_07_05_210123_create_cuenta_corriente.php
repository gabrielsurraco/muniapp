<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentaCorriente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuenta_corriente', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('NRO_MOV');
            $table->date('FECHA_MOVIMIENTO')->nullable();
            $table->decimal('DEBE',18,2)->default(0)->nullable();
            $table->decimal('HABER',18,2)->default(0)->nullable();
            $table->decimal('SALDO',18,2)->default(0)->nullable();
            $table->integer('FK_PROVEEDOR')->unsigned();
            $table->integer('FK_ORDEN_PAGO')->unsigned();
            $table->string('CONCEPTO')->nullable();
            $table->string('CREATED_BY')->nullable();
            $table->string('UPDATED_BY')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('cuenta_corriente');
    }
}
