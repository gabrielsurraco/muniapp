<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkOrdenesDePago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordenes_de_pago', function (Blueprint $table) {
            $table->foreign('FK_PROVEEDOR')->references('ID')->on('proveedores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordenes_de_pago', function (Blueprint $table) {
            $table->dropForeign('ordenes_de_pago_fk_proveedor_foreign');
        });
    }
}
