<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CUENTAS', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('CODIGO');
            $table->string('NOMBRE');
            $table->integer('NIVEL')->nullable();
            $table->integer('ID_CUENTA_SUPERIOR')->default(null)->nullable();
            $table->string('OBSERVACIONES')->nullable();
            $table->decimal('SALDO',18,2)->default(0)->nullable();
            $table->decimal('PRESUPUESTO',18,2)->default(0)->nullable();
            $table->decimal('PRESUPUESTO_LEGISLATIVO',18,2)->default(0)->nullable();
            $table->decimal('PRESUPUESTO_EJECUTIVO',18,2)->default(0)->nullable();
            $table->boolean('IS_PRINCIPAL',2)->default(false);
            $table->string('CREADO_POR')->nullable();
            $table->string('MODIFICADO_POR')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CUENTAS');
    }
}



