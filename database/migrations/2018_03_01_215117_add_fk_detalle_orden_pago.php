<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkDetalleOrdenPago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalles_ordenes_de_pago', function (Blueprint $table) {
            $table->foreign('FK_ORDEN_PAGO')->references('ID')->on('ORDENES_DE_PAGO');
            $table->foreign('FK_CUENTA')->references('ID')->on('CUENTAS');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalles_ordenes_de_pago', function (Blueprint $table) {
            $table->dropForeign('detalles_ordenes_de_pago_fk_orden_pago_foreign');
            $table->dropForeign('detalles_ordenes_de_pago_fk_cuenta_foreign');
        });
    }
}
