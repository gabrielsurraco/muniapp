<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBancosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('BANCOS', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('CODIGO');
            $table->string('NOMBRE');
            $table->string('DIRECCION')->nullable();
            $table->string('NUMERO_CUENTA');
            $table->date('CREATED_AT')->nullable();
            $table->date('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('BANCOS');
    }
}
