<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Chequeras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    
    
    {
        Schema::create('CHEQUERAS', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('NUMERO_CHEQUERA');
            $table->integer('DESDE');
            $table->integer('HASTA');
            $table->string('NOMBRE')->nullable();
            $table->integer('FK_BANCO')->unsigned();
            $table->string('CREATED_BY')->nullable();
            $table->string('UPDATED_BY')->nullable();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CHEQUERAS');
    }
}
