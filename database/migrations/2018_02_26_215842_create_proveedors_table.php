<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedores', function (Blueprint $table) {
            $table->increments('ID');
            $table->date('FECHA_CREACION')->nullable();
            $table->string('CODIGO')->nullable();
            $table->string('NOMBRE')->nullable();
            $table->string('RAZON_SOCIAL')->nullable();
            $table->string('CUIL',11);
            $table->string('DOMICILIO')->nullable();
            $table->string('TELEFONO')->nullable();
            $table->string('EMAIL')->nullable();
            $table->string('OBSERVACIONES')->nullable();
            $table->string('CREATED_BY')->nullable();
            $table->string('UPDATED_BY')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedores');
    }
}
