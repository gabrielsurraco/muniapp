<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ORDENES_DE_PAGO', function (Blueprint $table) {
            $table->increments('ID');
            $table->date('FECHA');
            $table->decimal('MONTO',18,2)->default(0)->nullable();
            $table->integer('FK_PROVEEDOR')->unsigned();
            $table->decimal('SALDO',18,2)->default(0)->nullable();
            $table->integer('ID_SUPERIOR')->default(0)->nullable();
            $table->integer('CODIGO');
            $table->integer('CODIGO_BIS')->default(0)->nullable();
            $table->string('NRO_COMP_RECI')->default(null)->nullable();
            $table->string('OBSERVACIONES')->default(null)->nullable();
            $table->string('CONCEPTO')->default(null)->nullable();
            $table->string('CREATED_BY')->default(null)->nullable();
            $table->string('UPDATED_BY')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ORDENES_DE_PAGO');
    }
}
