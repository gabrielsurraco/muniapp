<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleOrdenPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DETALLES_ORDENES_DE_PAGO', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('FK_ORDEN_PAGO')->unsigned();
            $table->integer('FK_CUENTA')->unsigned();
            $table->decimal('MONTO',18,2)->default(0);
            $table->enum('DEPARTAMENTO',array('EJECUTIVO','LEGISLATIVO'))->default(null)->nullable();
            $table->string('CREATED_BY')->default(null)->nullable();
            $table->string('UPDATED_BY')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DETALLES_ORDENES_DE_PAGO');
    }
}
