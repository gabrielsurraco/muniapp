<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecibosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RECIBOS', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('FK_PAGO')->unsigned();
            $table->date('FECHA');
            $table->decimal('SALDO',2);
            $table->string('CREATED_BY');
            $table->string('UPDATED_BY');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RECIBOS');
    }
}
